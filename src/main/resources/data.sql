INSERT INTO PAYMENT_MODE (id, mode) values (1, 'Prepaid') ON DUPLICATE KEY UPDATE id=1;
INSERT INTO PAYMENT_MODE (id, mode) values (2, 'COD') ON DUPLICATE KEY UPDATE id=2;


INSERT INTO CITY (id, name) values (1, 'Anakapalli') ON DUPLICATE KEY UPDATE id=1;
INSERT INTO AREA (id, area_name, city) values (1, 'Gavarapalem',1) ON DUPLICATE KEY UPDATE id=1;


INSERT INTO ROOT_CATEGORY (id, name, status) values (1, 'FOOD', 1) ON DUPLICATE KEY UPDATE id=1;
INSERT INTO CATEGORY(id, name, root_category) values (1, 'Checken Starters', 1) ON DUPLICATE KEY UPDATE id=1;
INSERT INTO CATEGORY(id, name, root_category) values (2, 'Veg Starters', 1) ON DUPLICATE KEY UPDATE id=2;

INSERT INTO ROOT_CATEGORY (id, name, status) values (2, 'GROCERY', 1) ON DUPLICATE KEY UPDATE id=2;
INSERT INTO CATEGORY(id, name, root_category) values (3, 'Dry Fruits', 1) ON DUPLICATE KEY UPDATE id=3;

INSERT INTO ROOT_CATEGORY (id, name, status) values (3, 'MEAT', 1) ON DUPLICATE KEY UPDATE id=3;
INSERT INTO CATEGORY(id, name, root_category) values (4, 'Chicken Skin Less', 3) ON DUPLICATE KEY UPDATE id=4;