package com.localbasket.exception;

public class ProductNotAvailableException extends Exception{
	private static final long serialVersionUID = 8234599385653274286L;
	public ProductNotAvailableException(String message) {
		super(message);
	}
}
