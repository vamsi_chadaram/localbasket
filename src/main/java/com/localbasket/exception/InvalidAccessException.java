package com.localbasket.exception;

public class InvalidAccessException extends Exception{
	private static final long serialVersionUID = 1L;

	public InvalidAccessException(String exception) {
		super(exception);
	}
}
