package com.localbasket.enumeration;

public enum OrderStatus {
	PLACED,
	IN_PROGRESS,
	CONFIRMED,
	REJECTED,
	OUT_FOR_DELIVERY,
	DELIVERED
}