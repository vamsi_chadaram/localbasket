package com.localbasket.listener;

import javax.annotation.Resource;

import org.springframework.context.ApplicationListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import com.localbasket.event.CustomerRegistrationEvent;
import com.localbasket.service.MailService;


@Component
public class CustomerRegistrationListener implements ApplicationListener<CustomerRegistrationEvent>{
	
	@Resource(name="customerMailService")
	private MailService mailService;
	
	@Override
	@Async
	public void onApplicationEvent(CustomerRegistrationEvent event) {
		mailService.sendEmail(event.getMessage());
	}
}
