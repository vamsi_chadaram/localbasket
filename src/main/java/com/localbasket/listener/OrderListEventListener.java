package com.localbasket.listener;

import javax.annotation.Resource;

import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import com.localbasket.event.OrdersListEvent;
import com.localbasket.service.MailService;

@Component
public class OrderListEventListener implements ApplicationListener<OrdersListEvent>{

	@Resource(name="orderListMailService")
	private MailService mailService;
	
	@Override
	public void onApplicationEvent(OrdersListEvent event) {
		mailService.sendEmail(event.getMessage());
	}
}


