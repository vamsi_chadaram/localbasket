package com.localbasket.listener;

import javax.annotation.Resource;

import org.springframework.context.ApplicationListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import com.localbasket.event.ForgotPasswordEvent;
import com.localbasket.service.MailService;


@Component
public class ForgotPasswordEventListener implements ApplicationListener<ForgotPasswordEvent>{
	
	@Resource(name="forgotPasswordEmailService")
	private MailService mailService;
	
	@Override
	@Async
	public void onApplicationEvent(ForgotPasswordEvent event) {
		mailService.sendEmail(event.getMessage());
	}

}
