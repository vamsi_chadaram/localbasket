package com.localbasket.listener;

import javax.annotation.Resource;

import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import com.localbasket.event.OrderConfirmationEvent;
import com.localbasket.service.MailService;

@Component
public class OrderConfirmationListener implements ApplicationListener<OrderConfirmationEvent>{

	@Resource(name="orderConfirmationMailService")
	private MailService mailService;
	
	@Override
	public void onApplicationEvent(OrderConfirmationEvent event) {
		mailService.sendEmail(event.getMessage());
	}
}
