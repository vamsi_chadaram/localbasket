package com.localbasket.service;

import com.localbasket.data.MailData;

public interface MailService {
	void sendEmail(MailData mail);
}
