package com.localbasket.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.localbasket.model.BrandCategoryModel;
import com.localbasket.model.CategoryModel;
import com.localbasket.model.ProductModel;
import com.localbasket.model.StoreModel;

public interface ProductService extends GenericService<ProductModel>{
	ProductModel findByIdAndStore(Long id, StoreModel store);
	List<ProductModel> findByStore(StoreModel storeModel);
	Page<ProductModel> findByCategoryAndStore(CategoryModel category,StoreModel store, Pageable paging);
	Page<ProductModel> findByCategoryAndStoreAndBrandCategory(CategoryModel category,StoreModel store,BrandCategoryModel brandCategory, Pageable paging);
}
