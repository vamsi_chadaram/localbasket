package com.localbasket.service;

import com.localbasket.model.ReviewModel;

public interface ReviewService extends GenericService<ReviewModel>{
}
