package com.localbasket.service;


import com.localbasket.model.CustomerModel;
import com.localbasket.model.UserModel;

public interface CustomerService extends GenericService<CustomerModel>{
	CustomerModel findByMobile(String mobile);
	UserModel getCurrentUser();
	CustomerModel findByUsername(String userName);
	boolean findByReferralCode(String referralCode);
}
