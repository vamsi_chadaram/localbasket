package com.localbasket.service;

import java.util.List;

import com.localbasket.model.CityModel;
import com.localbasket.model.ZoneModel;

public interface ZoneService {
	ZoneModel findById(Long zoneId);
	List<ZoneModel> getZonesByCity(CityModel city);
}
