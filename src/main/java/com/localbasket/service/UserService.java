package com.localbasket.service;

import com.localbasket.model.UserModel;

public interface UserService {
	UserModel findByEmail(String username);
	UserModel findByMobile(String mobile);
	boolean findByReferralCode(String referralCode);
}
