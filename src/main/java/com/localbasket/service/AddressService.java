package com.localbasket.service;

import java.util.List;
import com.localbasket.model.AddressModel;
import com.localbasket.model.AreaModel;
import com.localbasket.model.CityModel;
import com.localbasket.model.CustomerAddressModel;
import com.localbasket.model.CustomerModel;

public interface AddressService extends GenericService<AddressModel> {
	List<AddressModel> getCustomerAddressById(Long customerId);

	AreaModel save(AreaModel area);

	List<AreaModel> getAreasByCity(Long cityId);

	CityModel save(CityModel city);

	List<CityModel> getCities();

	CustomerAddressModel saveCustomerAddress(CustomerAddressModel customerAddress);

	List<CustomerAddressModel> getCustomerAddresses(CustomerModel customer);

	CustomerAddressModel getCustomerAddress(Long customerAddressId);

	void removeCustomerAddress(CustomerAddressModel customerAddress);

	CityModel findCityById(Long id);
}
