package com.localbasket.service;

import java.util.List;

import com.localbasket.model.AbstractOrderEntryModel;
import com.localbasket.model.ProductModel;

public interface OrderEntryService extends GenericService<AbstractOrderEntryModel>{
	AbstractOrderEntryModel findByProduct(ProductModel product, Long cartId);
	List<AbstractOrderEntryModel> findByCart(Long cartId);
}
