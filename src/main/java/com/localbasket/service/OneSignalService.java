package com.localbasket.service;

import java.util.List;

import com.localbasket.model.OneSignalModel;

public interface OneSignalService {
	OneSignalModel createOneSignal(OneSignalModel OneSignal);
	OneSignalModel getOneSignal(Long id);
	List<OneSignalModel> findAll();
	public void delete(Long id);
}
