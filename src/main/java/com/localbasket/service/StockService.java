package com.localbasket.service;

import com.localbasket.exception.OrderNotFoundException;
import com.localbasket.model.StockLevelModel;

public interface StockService extends GenericService<StockLevelModel>{
	boolean isAvailable(Long productId) throws OrderNotFoundException;
	Integer getAvailableCount(Long productId) throws OrderNotFoundException;
}
