package com.localbasket.service;

import java.util.Date;
import java.util.List;

import com.localbasket.enumeration.OrderStatus;
import com.localbasket.exception.StockUnavailableException;
import com.localbasket.model.CartModel;
import com.localbasket.model.CustomerModel;
import com.localbasket.model.DeliveryPersonModel;
import com.localbasket.model.OrderModel;
import com.localbasket.model.StoreModel;

public interface OrderService extends GenericService<OrderModel>{
	OrderModel placeOrder(CartModel cart) throws CloneNotSupportedException, StockUnavailableException;
	List<OrderModel> findOrdersByStore(StoreModel store);
	List<OrderModel> findOrdersByStoreAndOrderStatus(StoreModel store, OrderStatus orderStatus);
	List<OrderModel> findByCustomer(CustomerModel customer);
	List<OrderModel> getAllBetweenDates(Date startDate, Date endDate);
	List<OrderModel> getPendingOrders(DeliveryPersonModel deliveryPerson);
	List<OrderModel> getOrdersByDeliveryBoyWithDates(DeliveryPersonModel deliveryPerson, Date startDate, Date endDate);
	List<OrderModel> getPendingOrders(DeliveryPersonModel deliveryPerson, Date startDate, Date endDate, OrderStatus orderStatus);
}
