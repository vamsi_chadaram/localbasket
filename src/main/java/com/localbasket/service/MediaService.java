package com.localbasket.service;

import com.localbasket.model.MediaContainer;
import com.localbasket.model.MediaModel;

public interface MediaService {
	MediaModel createMedia(MediaModel media);
	MediaContainer createMediaContainer(MediaContainer mediaContainer);
}
