package com.localbasket.service.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import com.localbasket.dao.OrderDao;
import com.localbasket.data.MailData;
import com.localbasket.enumeration.OrderStatus;
import com.localbasket.event.OrderConfirmationEventPublisher;
import com.localbasket.exception.StockUnavailableException;
import com.localbasket.firebase.integration.PushnotificationClient;
import com.localbasket.model.AbstractOrderEntryModel;
import com.localbasket.model.AddressModel;
import com.localbasket.model.AreaModel;
import com.localbasket.model.CartModel;
import com.localbasket.model.CustomerModel;
import com.localbasket.model.DeliveryPersonModel;
import com.localbasket.model.OrderModel;
import com.localbasket.model.ProductModel;
import com.localbasket.model.StockLevelModel;
import com.localbasket.model.StoreModel;
import com.localbasket.service.CartService;
import com.localbasket.service.DeliveryPersonService;
import com.localbasket.service.OrderEntryService;
import com.localbasket.service.OrderService;
import com.localbasket.service.StockService;

@Service
public class DefaultOrderService implements OrderService{

	@Autowired
	private OrderDao orderDao;
	
	@Autowired
	private CartService cartService;
	
	@Autowired
	private StockService stockService;
	
	@Autowired
	private DeliveryPersonService deliveryPersonService;
	
	@Autowired
	private OrderEntryService orderEntryService;
	
	@Autowired
	private OrderConfirmationEventPublisher orderConfirmationEventPublisher;
	
	@Autowired
	private PushnotificationClient pushNotificationClient;
	
	@Override
	public OrderModel save(OrderModel model) {
		sendNotification(model);
		return orderDao.save(model);
	}

	@Override
	public OrderModel findById(Long id) {
		Optional<OrderModel> order= orderDao.findById(id);
		return order.get();
	}

	@Override
	public Page<OrderModel> findAll(Pageable paging) {
		return orderDao.findAll(paging);
	}

	@Override
	public Iterable<OrderModel> findAll() {
		return orderDao.findAll();
	}

	@Override
	public OrderModel placeOrder(CartModel cart) throws CloneNotSupportedException, StockUnavailableException {
		CartModel clonedCart= (CartModel) cart.clone();
		
		Set<AbstractOrderEntryModel> orderEnties= new HashSet<AbstractOrderEntryModel>();
		orderEnties.addAll(orderEntryService.findByCart(clonedCart.getId()));
		
		beforePlaceOrder();
		
		OrderModel orderModel= new OrderModel();
		orderModel.setCreatedDate(new Date());
		orderModel.setCustomer(clonedCart.getCustomer());
		orderModel.setDeliveryAddress(clonedCart.getDeliveryAddress());
		orderModel.setDeliveryCharges(clonedCart.getDeliveryCharges());
		orderModel.setDiscount(clonedCart.getDiscount());
		orderModel.setOrderEntries(orderEnties);
		orderModel.setPaymentAddress(clonedCart.getPaymentAddress());
		orderModel.setPaymentMode(clonedCart.getPaymentMode());
		
		if(clonedCart.getStore().getRootCategory().getName().equals("FRESH ZONE")) {
			orderModel.setOrderStatus(OrderStatus.CONFIRMED);
		} else {
			orderModel.setOrderStatus(OrderStatus.PLACED);
		}
		
		orderModel.setStore(clonedCart.getStore());
		orderModel.setSubTotal(clonedCart.getSubTotal());
		orderModel.setTotal(clonedCart.getTotal());
		OrderModel order= orderDao.save(orderModel);
		
		afterPlaceOrder(order);
		
		cartService.remove(cart);
		return order;
	}

	private void beforePlaceOrder() throws StockUnavailableException {
		CartModel cartModel=cartService.getCurrentCart();
		
		List<AbstractOrderEntryModel> orderEntries= orderEntryService.findByCart(cartModel.getId());
		for(AbstractOrderEntryModel orderEntry: orderEntries) {
			ProductModel product= orderEntry.getProduct();
			StockLevelModel stockLevel= product.getStockLevel();
			boolean forceStock=stockLevel.isForceAvailable();
			if(!forceStock) {
				if(stockLevel.getAvailableCount() <= 0) {
					throw new StockUnavailableException("Product went out of stock");
				}
			} 
		}
	}
	
	
	private void afterPlaceOrder(OrderModel order) throws StockUnavailableException {
		
		Set<AbstractOrderEntryModel> orderEntries= order.getOrderEntries();
		for(AbstractOrderEntryModel orderEntry: orderEntries) {
			ProductModel product= orderEntry.getProduct();
			StockLevelModel stockLevel= product.getStockLevel();
			
			if(!stockLevel.isForceAvailable()) {
				Integer qty= orderEntry.getQuantity();
				Integer existingQty=stockLevel.getAvailableCount();
				Integer remainingQty= existingQty-qty;
				stockLevel.setAvailableCount(remainingQty);
				stockService.save(stockLevel);
			}
		}
		
		AddressModel addressModel=order.getDeliveryAddress();
		AreaModel area= addressModel.getArea();
		List<DeliveryPersonModel> deliveryPersons=deliveryPersonService.findByArea(area);
		
		for(DeliveryPersonModel deliveryPerson:deliveryPersons) {
			if(deliveryPerson.isAvailable()) {
				order.setDeliveryPerson(deliveryPerson);
			}
		}
		
		try {
			CustomerModel customer=order.getCustomer();
			MailData mailData = new MailData();
			mailData.setMailTo(customer.getEmail());
			mailData.setMailSubject("LocalBasket HD");
			mailData.setMailFrom("sales@localbaskethd.com");
			Map<String, Object> propertiesMap= new HashMap<>();
			propertiesMap.put("order", order);
			mailData.setModel(propertiesMap);
			orderConfirmationEventPublisher.publishCustomEvent(mailData);
		}catch(Exception ex) {
			
		}
		
		try {
			pushNotificationClient.sendOrderNotification(order);
		} catch(Exception ex) {
			
		}
		orderDao.save(order);
	}
	
	@Override
	public void remove(OrderModel t) {
		
	}

	@Override
	public List<OrderModel> findOrdersByStore(StoreModel store) {
		return orderDao.findByStore(store);
	}

	@Override
	public List<OrderModel> findOrdersByStoreAndOrderStatus(StoreModel store, OrderStatus orderStatus) {
		return orderDao.findOrdersByStoreAndOrderStatus(store,orderStatus);
	}

	@Override
	public List<OrderModel> findByCustomer(CustomerModel customer) {
		return orderDao.findByCustomer(customer);
	}

	@Override
	public List<OrderModel> getAllBetweenDates(Date startDate, Date endDate) {
		List<OrderModel> orders= orderDao.getAllBetweenDates(startDate, endDate);
		try {
			MailData mail= new MailData();
			Map < String, Object > model= new HashMap<>();
			model.put("orders", orders);
			orderConfirmationEventPublisher.sendOrdersBetweenDates(mail);
		}catch(Exception ex) {
			
		}
		return orders;
	}
	
	public void sendNotification(OrderModel order) {
		if(!order.getOrderStatus().equals(OrderStatus.IN_PROGRESS)) {
			try {
				pushNotificationClient.sendOrderConfirmation(order);
			}catch(Exception ex) {
				//TODD
			}
		}
	}

	@Override
	public List<OrderModel> getPendingOrders(DeliveryPersonModel deliveryPerson) {
		return orderDao.findByDeliveryPerson(deliveryPerson);
	}
	
	@Override
	public List<OrderModel> getOrdersByDeliveryBoyWithDates(DeliveryPersonModel deliveryPerson, Date startDate, Date endDate){
		return orderDao.findByDeliveryPerson(deliveryPerson, startDate, endDate);
	}
	
	@Override
	public List<OrderModel> getPendingOrders(DeliveryPersonModel deliveryPerson, Date startDate, Date endDate, OrderStatus orderStatus){
		return orderDao.pendingOrdersByDeliveryPerson(deliveryPerson,startDate,endDate,orderStatus);
	}
}