package com.localbasket.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import com.localbasket.dao.ProductDao;
import com.localbasket.model.BrandCategoryModel;
import com.localbasket.model.CategoryModel;
import com.localbasket.model.ProductModel;
import com.localbasket.model.StoreModel;
import com.localbasket.service.ProductService;
import com.localbasket.service.StoreService;

@Service
public class DefaultProductService implements ProductService{

	@Autowired
	private StoreService storeService;
	
	@Autowired
	private ProductDao productDao;
	
	@Override
	public ProductModel save(ProductModel model) {
		return productDao.save(model);
	}

	@Override
	public ProductModel findById(Long id) {
		ProductModel product= productDao.findByIdAndStatus(id, true);
		
		if(null != storeService.getCurrentStoreUser() && null == product) {
			return productDao.findByIdAndStatus(id, false);
		}
		return product;
	}

	@Override
	public Page<ProductModel> findAll(Pageable paging) {
		return productDao.findAll(paging);
	}

	@Override
	public Iterable<ProductModel> findAll() {
		return productDao.findAll();
	}

	@Override
	public List<ProductModel> findByStore(StoreModel storeModel) {
		return productDao.findByStore(storeModel);
	}

	@Override
	public void remove(ProductModel t) {
		
	}

	@Override
	public Page<ProductModel> findByCategoryAndStore(CategoryModel category,StoreModel store,Pageable paging) {
		return productDao.findByCategoryAndStore(category,store, paging);
	}

	@Override
	public Page<ProductModel> findByCategoryAndStoreAndBrandCategory(CategoryModel category, StoreModel store,BrandCategoryModel brandCategory,
			Pageable paging) {
		return productDao.findByCategoryAndStoreAndBrandCategory(category, store, brandCategory, paging);
	}

	@Override
	public ProductModel findByIdAndStore(Long id, StoreModel store) {
		return productDao.findByIdAndStore(id,store);
	}
}
