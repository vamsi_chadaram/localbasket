package com.localbasket.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import com.localbasket.dao.AddressDao;
import com.localbasket.dao.AreaDao;
import com.localbasket.dao.CityDao;
import com.localbasket.dao.CustomerAddressDao;
import com.localbasket.model.AddressModel;
import com.localbasket.model.AreaModel;
import com.localbasket.model.CityModel;
import com.localbasket.model.CustomerAddressModel;
import com.localbasket.model.CustomerModel;
import com.localbasket.service.AddressService;
import com.localbasket.service.CustomerService;

@Service
public class DefaultAddressService implements AddressService {

	@Autowired
	private AddressDao addressDao;

	@Autowired
	private CustomerService customerService;

	@Autowired
	private CustomerAddressDao customerAddressDao;

	@Autowired
	private CityDao cityDao;

	@Autowired
	private AreaDao areaDao;

	@Override
	public AddressModel save(AddressModel model) {
		return addressDao.save(model);
	}

	@Override
	public void removeCustomerAddress(CustomerAddressModel customerAddress) {
		customerAddressDao.delete(customerAddress);
	}

	@Override
	public AddressModel findById(Long id) {
		Optional<AddressModel> address = addressDao.findById(id);
		if (!address.isPresent()) {

		}
		return address.get();
	}

	@Override
	public Page<AddressModel> findAll(Pageable paging) {
		return addressDao.findAll(paging);
	}

	@Override
	public Iterable<AddressModel> findAll() {
		return addressDao.findAll();
	}

	@Override
	public List<AddressModel> getCustomerAddressById(Long customerId) {
		CustomerModel customerModel = customerService.findById(customerId);
		List<CustomerAddressModel> customerAddressList = customerAddressDao.findByCustomer(customerModel);

		List<AddressModel> addressList = new ArrayList<AddressModel>();

		if (CollectionUtils.isEmpty(customerAddressList)) {
			return Collections.emptyList();
		}
		for (CustomerAddressModel customerAddress : customerAddressList) {
			addressList.add(customerAddress.getAddress());
		}
		return addressList;
	}

	@Override
	public AreaModel save(AreaModel area) {
		return areaDao.save(area);
	}

	@Override
	public List<AreaModel> getAreasByCity(Long cityId) {
		Optional<CityModel> cityModel = cityDao.findById(cityId);
		return areaDao.findByCity(cityModel.get());
	}

	@Override
	public CityModel save(CityModel city) {
		return cityDao.save(city);
	}

	@Override
	public List<CityModel> getCities() {
		return cityDao.findAll();
	}

	@Override
	public CustomerAddressModel saveCustomerAddress(CustomerAddressModel customerAddress) {

		if (customerAddress.isDefaultAddress()) {
			CustomerAddressModel customerAddressModel = customerAddressDao.findByDefaultAddress(true);
			customerAddressModel.setDefaultAddress(false);
			customerAddressDao.save(customerAddressModel);
		}
		return customerAddressDao.save(customerAddress);
	}

	@Override
	public void remove(AddressModel t) {
	}

	@Override
	public List<CustomerAddressModel> getCustomerAddresses(CustomerModel customer) {
		return customerAddressDao.findByCustomer(customer);
	}

	@Override
	public CustomerAddressModel getCustomerAddress(Long customerAddressId) {
		return customerAddressDao.findById(customerAddressId).get();
	}

	@Override
	public CityModel findCityById(Long id) {
		return cityDao.findById(id).get();
	}

}
