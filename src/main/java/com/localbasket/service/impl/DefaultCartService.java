package com.localbasket.service.impl;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.localbasket.cart.data.CommerceCartData;
import com.localbasket.dao.CartDao;
import com.localbasket.model.AbstractOrderEntryModel;
import com.localbasket.model.CartModel;
import com.localbasket.model.CustomerModel;
import com.localbasket.model.DeliveryModeModel;
import com.localbasket.model.PriceRowModel;
import com.localbasket.model.ProductModel;
import com.localbasket.model.StoreModel;
import com.localbasket.service.CartService;
import com.localbasket.service.CustomerService;
import com.localbasket.service.DeliveryModeService;
import com.localbasket.service.OrderEntryService;
import com.localbasket.service.ProductService;

@Service
public class DefaultCartService implements CartService {

	@Autowired
	private CartDao cartDao;

	@Autowired
	private ProductService productService;

	@Autowired
	private CustomerService customerService;

	@Autowired
	private OrderEntryService orderEntryService;

	@Autowired
	private DeliveryModeService deliveryModeService;
	
	@Override
	public CartModel addToCart(CommerceCartData commerceCart) {
		ProductModel product = productService.findById(commerceCart.getProductCode());
		StoreModel store = product.getStore();

		CustomerModel customer = (CustomerModel) customerService.getCurrentUser();
		CartModel cartModel = getCart(store);
		cartModel.setCustomer(customer);

		AbstractOrderEntryModel abstractOrderEntryModel = orderEntryService.findByProduct(product, cartModel.getId());

		if (null == abstractOrderEntryModel) {
			abstractOrderEntryModel = createOrderEntry(commerceCart, product, cartModel);
		} else {

			if (abstractOrderEntryModel.getQuantity() == 1 && commerceCart.getQty() < 1) {
				orderEntryService.remove(abstractOrderEntryModel);
			} else {
				abstractOrderEntryModel.setQuantity(abstractOrderEntryModel.getQuantity() + commerceCart.getQty());
			}
		}
		return calculateCart(cartModel);
	}

	private AbstractOrderEntryModel createOrderEntry(CommerceCartData commerceCart, ProductModel product,
			CartModel cartModel) {
		AbstractOrderEntryModel aoem = new AbstractOrderEntryModel();
		aoem.setProduct(product);
		aoem.setQuantity(commerceCart.getQty());
		PriceRowModel priceRow = product.getPriceRow();
		double subtotal = priceRow.getDiscountedPrice().doubleValue() * commerceCart.getQty();
		aoem.setSubTotal(subtotal);
		aoem.setCart(cartModel.getId());
		orderEntryService.save(aoem);
		return aoem;
	}

	@Override
	public CartModel save(CartModel model) {
		return cartDao.save(model);
	}

	@Override
	public CartModel findById(Long id) {
		Optional<CartModel> cart = cartDao.findById(id);

		if (!cart.isPresent()) {

		}
		return cart.get();
	}

	@Override
	public Page<CartModel> findAll(Pageable paging) {
		return cartDao.findAll(paging);
	}

	@Override
	public Iterable<CartModel> findAll() {
		return cartDao.findAll();
	}

	@Override
	public List<CartModel> findByCustomerAndStore(CustomerModel customer, StoreModel store) {
		return cartDao.findByCustomerAndStore(customer, store);
	}

	private CartModel calculateCart(CartModel cartModel) {
		if (null == cartModel.getId() || CollectionUtils.isEmpty(orderEntryService.findByCart(cartModel.getId()))) {
			cartModel.setSubTotal(Double.valueOf(0));
			cartModel.setTotal(Double.valueOf(0));
			cartModel.setTotalQuantity(0);
			cartModel.setDeliveryCharges(Double.valueOf(0));
			return save(cartModel);
		}

		double subtotal = 0.0;
		int quantity = 0;

		StoreModel storeModel = cartModel.getStore();
		List<AbstractOrderEntryModel> orderEntries = orderEntryService.findByCart(cartModel.getId());

		for (AbstractOrderEntryModel orderEntry : orderEntries) {
			ProductModel productModel = orderEntry.getProduct();
			PriceRowModel priceRow = productModel.getPriceRow();
			if (null != priceRow.getDiscountedPrice()) {
				subtotal = (priceRow.getDiscountedPrice().doubleValue() * orderEntry.getQuantity()) + subtotal;
			}
			quantity = quantity + orderEntry.getQuantity();
		}
		Double deliveryCharges = 0.0;
		if (null != storeModel.getDeliveryMode()) {
			deliveryCharges = storeModel.getDeliveryMode().getCharges() * quantity;
			cartModel.setDeliveryText(storeModel.getDeliveryMode().getDeliveryText());
		} else if(null != cartModel.getDeliveryCharges()) {
			deliveryCharges = deliveryCharges+ cartModel.getDeliveryCharges();
		}
		double total = 0.0;
		total = subtotal + (deliveryCharges);
		cartModel.setSubTotal(subtotal);
		cartModel.setTotal(total);
		cartModel.setTotalQuantity(quantity);
		cartModel.setDeliveryCharges(deliveryCharges);
		return save(cartModel);
	}

	@Override
	public CartModel getCurrentCart() {
		CustomerModel customer = (CustomerModel) customerService.getCurrentUser();
		CartModel cartModel = findByCustomer(customer);
		if (null == cartModel) {
			cartModel = new CartModel();
			cartModel.setCreatedDate(new Date());
			return cartModel;
		}
		return calculateCart(cartModel);
	}

	private CartModel getCart(StoreModel store) {
		CartModel cart = getCurrentCart();
		if (null == cart.getStore()) {
			cart.setStore(store);
			if(null != store.getDeliveryMode() && null != store.getDeliveryMode().getCharges()) {
				cart.setDeliveryCharges(store.getDeliveryMode().getCharges());
			} else {
				DeliveryModeModel deliveryMode=deliveryModeService.getDeliveryMode("delivery_charges");
				cart.setDeliveryCharges(deliveryMode.getCharges());
			}
		} else if (cart.getStore().getId() != store.getId()) {
			cart.setStore(store);
		}
		return cartDao.save(cart);
	}

	@Override
	public CartModel findByCustomer(CustomerModel customer) {
		return cartDao.findByCustomer(customer);
	}

	@Override
	public void remove(CartModel cart) {
		cartDao.delete(cart);
	}
}
