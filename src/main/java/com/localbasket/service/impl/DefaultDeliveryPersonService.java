package com.localbasket.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.localbasket.dao.DeliveryPersonDao;
import com.localbasket.dao.OrderDao;
import com.localbasket.model.AreaModel;
import com.localbasket.model.DeliveryPersonModel;
import com.localbasket.model.OrderModel;
import com.localbasket.service.DeliveryPersonService;

@Service
public class DefaultDeliveryPersonService implements DeliveryPersonService{

	@Autowired
	private DeliveryPersonDao deliveryPersonDao;
	@Autowired
	private OrderDao orderDao;
	
	@Override
	public DeliveryPersonModel save(DeliveryPersonModel model) {
		model.setUserType("DELIVERY_PERSON");
		return deliveryPersonDao.save(model);
	}

	@Override
	public DeliveryPersonModel findById(Long id) {
		Optional<DeliveryPersonModel> deliveryPerson=deliveryPersonDao.findById(id);
		return deliveryPerson.get();
	}

	@Override
	public Page<DeliveryPersonModel> findAll(Pageable paging) {
		return null;
	}

	@Override
	public Iterable<DeliveryPersonModel> findAll() {
		return deliveryPersonDao.findAll();
	}

	@Override
	public void remove(DeliveryPersonModel t) {
		
	}

	@Override
	public DeliveryPersonModel findByEmailAndActive(String email, boolean active) {
		return deliveryPersonDao.findByEmailAndActive(email, active);
	}

	@Override
	public List<DeliveryPersonModel> findByArea(AreaModel areaModel) {
		return deliveryPersonDao.findByArea(areaModel);
	}

	@Override
	public List<OrderModel> findByDeliveryPerson(DeliveryPersonModel deliveryPerson) {
		return orderDao.findByDeliveryPerson(deliveryPerson);
	}

	@Override
	public DeliveryPersonModel findByEmail(String email) {
		return deliveryPersonDao.findByEmail(email);
	}
}
