package com.localbasket.service.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.localbasket.dao.ReviewDao;
import com.localbasket.model.ReviewModel;
import com.localbasket.service.ReviewService;

@Service
public class DefaultReviewService implements ReviewService{

	@Autowired
	private ReviewDao reviewDao;
	
	@Override
	public ReviewModel save(ReviewModel model) {
		return reviewDao.save(model);
	}

	@Override
	public ReviewModel findById(Long id) {
		Optional<ReviewModel> review= reviewDao.findById(id);
		return review.get();
	}

	@Override
	public Page<ReviewModel> findAll(Pageable paging) {
		return reviewDao.findAll(paging);
	}

	@Override
	public Iterable<ReviewModel> findAll() {
		return reviewDao.findAll();
	}

	@Override
	public void remove(ReviewModel t) {
		
	}
}
