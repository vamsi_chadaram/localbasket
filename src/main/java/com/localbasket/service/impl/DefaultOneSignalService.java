package com.localbasket.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.localbasket.dao.OneSignalDao;
import com.localbasket.model.OneSignalModel;
import com.localbasket.service.OneSignalService;

@Service
public class DefaultOneSignalService implements OneSignalService{

	@Autowired
	private OneSignalDao oneSignalDao;
	@Override
	public OneSignalModel createOneSignal(OneSignalModel oneSignal) {
		return oneSignalDao.save(oneSignal);
	}

	@Override
	public OneSignalModel getOneSignal(Long id) {
		Optional<OneSignalModel> oneSignal=oneSignalDao.findById(id);
		if(oneSignal.isPresent()) {
			return oneSignal.get();
		}
		return null;
	}

	@Override
	public List<OneSignalModel> findAll() {
		return oneSignalDao.findAll();
	}
	@Override
	public void delete(Long id) {
		OneSignalModel oneSignalModel=getOneSignal(id);
		oneSignalDao.delete(oneSignalModel);
	}

}
