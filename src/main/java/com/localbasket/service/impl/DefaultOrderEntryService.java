package com.localbasket.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.localbasket.dao.OrderEntryDao;
import com.localbasket.model.AbstractOrderEntryModel;
import com.localbasket.model.ProductModel;
import com.localbasket.service.OrderEntryService;

@Service
public class DefaultOrderEntryService implements OrderEntryService{

	@Autowired
	private OrderEntryDao orderEntryDao;
	
	@Override
	public AbstractOrderEntryModel save(AbstractOrderEntryModel model) {
		return orderEntryDao.save(model);
	}

	@Override
	public AbstractOrderEntryModel findById(Long id) {
		return null;
	}

	@Override
	public Page<AbstractOrderEntryModel> findAll(Pageable paging) {
		return null;
	}

	@Override
	public Iterable<AbstractOrderEntryModel> findAll() {
		return null;
	}

	@Override
	public void remove(AbstractOrderEntryModel t) {
		orderEntryDao.delete(t);
	}

	@Override
	public AbstractOrderEntryModel findByProduct(ProductModel product, Long cartId) {
		return orderEntryDao.findByProductAndCart(product, cartId);
	}

	@Override
	public List<AbstractOrderEntryModel> findByCart(Long cartId) {
		return orderEntryDao.findByCart(cartId);
	}
}
