package com.localbasket.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.localbasket.dao.BrandCategoryDao;
import com.localbasket.model.BrandCategoryModel;
import com.localbasket.model.CategoryModel;
import com.localbasket.service.BrandCategoryService;

@Service
public class DefaultBrandCategoryService implements BrandCategoryService{

	@Autowired
	private BrandCategoryDao brandCategoryDao;
	
	@Override
	public BrandCategoryModel save(BrandCategoryModel model) {
		return brandCategoryDao.save(model);
	}

	@Override
	public BrandCategoryModel findById(Long id) {
		Optional<BrandCategoryModel> brandCategory=brandCategoryDao.findById(id);
		return brandCategory.get();
	}

	@Override
	public Page<BrandCategoryModel> findAll(Pageable paging) {
		return brandCategoryDao.findAll(paging);
	}

	@Override
	public Iterable<BrandCategoryModel> findAll() {
		return brandCategoryDao.findAll();
	}

	@Override
	public void remove(BrandCategoryModel t) {
		brandCategoryDao.delete(t);
	}

	@Override
	public List<BrandCategoryModel> findByCategory(CategoryModel category) {
		return brandCategoryDao.findByCategory(category);
	}

}
