package com.localbasket.service.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.localbasket.dao.PriceRowDao;
import com.localbasket.model.PriceRowModel;
import com.localbasket.service.PriceRowService;

@Service
public class DefaultPriceRowService implements PriceRowService{

	@Autowired
	private PriceRowDao priceRowDao;
	
	@Override
	public PriceRowModel save(PriceRowModel model) {
		return priceRowDao.save(model);
	}

	@Override
	public PriceRowModel findById(Long id) {
		Optional<PriceRowModel> priceRow=priceRowDao.findById(id);
		if(!priceRow.isPresent()) {
			
		}
		return priceRow.get();
	}

	@Override
	public Page<PriceRowModel> findAll(Pageable paging) {
		return priceRowDao.findAll(paging);
	}

	@Override
	public Iterable<PriceRowModel> findAll() {
		return priceRowDao.findAll();
	}

	@Override
	public void remove(PriceRowModel t) {
		// TODO Auto-generated method stub
		
	}

}
