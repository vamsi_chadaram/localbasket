package com.localbasket.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.localbasket.dao.CategoryDao;
import com.localbasket.exception.OrderNotFoundException;
import com.localbasket.model.CategoryModel;
import com.localbasket.model.RootCategoryModel;
import com.localbasket.service.CategoryService;
import com.localbasket.service.RootCategoryService;

@Service
public class DefaultCategoryService implements CategoryService{

	@Autowired
	private CategoryDao categoryDao;
	
	@Autowired
	private RootCategoryService rootCategoryService;
	
	@Override
	public CategoryModel save(CategoryModel model) {
		return categoryDao.save(model);
	}

	@Override
	public CategoryModel findById(Long id) {
		Optional<CategoryModel> category= categoryDao.findById(id);
		if(!category.isPresent()) {
			
		}
		return category.get();
	}

	@Override
	public Page<CategoryModel> findAll(Pageable paging) {
		return categoryDao.findAll(paging);
	}

	@Override
	public Iterable<CategoryModel> findAll() {
		return categoryDao.findAll();
	}

	@Override
	public void remove(CategoryModel t) {
		
	}

	@Override
	public List<CategoryModel> listByRootCategory(Long rootCategoryId) throws OrderNotFoundException {
		RootCategoryModel rootCategory= rootCategoryService.findById(rootCategoryId);
		return categoryDao.findByRootCategory(rootCategory);
	}
}
