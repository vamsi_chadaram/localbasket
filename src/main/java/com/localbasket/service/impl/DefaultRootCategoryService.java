package com.localbasket.service.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.localbasket.dao.RootCategoryDao;
import com.localbasket.model.RootCategoryModel;
import com.localbasket.service.RootCategoryService;

@Service
public class DefaultRootCategoryService implements RootCategoryService{

	@Autowired
	private RootCategoryDao rootCategoryDao;
	
	@Override
	public RootCategoryModel save(RootCategoryModel model) {
		return rootCategoryDao.save(model);
	}

	@Override
	public RootCategoryModel findById(Long id) {
		Optional<RootCategoryModel> rootCategory=rootCategoryDao.findById(id);
		if(!rootCategory.isPresent()) {
			
		}
		return rootCategory.get();
	}

	@Override
	public Page<RootCategoryModel> findAll(Pageable paging) {
		return null;
	}

	@Override
	public Iterable<RootCategoryModel> findAll() {
		return rootCategoryDao.findAll();
	}

	@Override
	public void remove(RootCategoryModel t) {
		// TODO Auto-generated method stub
		
	}

}
