package com.localbasket.service.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.localbasket.dao.StockLevelDao;
import com.localbasket.exception.OrderNotFoundException;
import com.localbasket.model.ProductModel;
import com.localbasket.model.StockLevelModel;
import com.localbasket.service.ProductService;
import com.localbasket.service.StockService;

@Service
public class DefaultStockService implements StockService{

	@Autowired
	private StockLevelDao stockLevelDao;
	
	@Autowired
	private ProductService productService;
	
	@Override
	public StockLevelModel save(StockLevelModel model) {
		return stockLevelDao.save(model);
	}

	@Override
	public StockLevelModel findById(Long id) {
		Optional<StockLevelModel> stockLevel=stockLevelDao.findById(id);
		if(!stockLevel.isPresent()) {
			
		}
		return stockLevel.get();
	}

	@Override
	public Page<StockLevelModel> findAll(Pageable paging) {
		return stockLevelDao.findAll(paging);
	}

	@Override
	public Iterable<StockLevelModel> findAll() {
		return stockLevelDao.findAll();
	}

	@Override
	public void remove(StockLevelModel t) {
		
	}

	@Override
	public boolean isAvailable(Long productId) throws OrderNotFoundException {
		ProductModel product=productService.findById(productId);
		StockLevelModel stockLevelModel=product.getStockLevel();
		if(stockLevelModel.isForceAvailable() || (stockLevelModel.getAvailableCount() > 0)) {
			return true;
		}
		return false;
	}
	
	@Override
	public Integer getAvailableCount(Long productId) throws OrderNotFoundException {
		ProductModel product=productService.findById(productId);
		StockLevelModel stockLevelModel=product.getStockLevel();
		return stockLevelModel.getAvailableCount();
	}
}
