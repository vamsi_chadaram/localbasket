package com.localbasket.service.impl;

import java.io.IOException;
import java.util.Date;

import org.apache.velocity.shaded.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.localbasket.model.MediaModel;
import com.localbasket.service.ImageStorageService;
import com.localbasket.service.MediaService;

@Service("imageStorageService")
public class ImageStorageServiceImpl implements ImageStorageService{


	private String FOLDER = "files/";

	@Autowired
	private AmazonS3 s3Client;

	@Autowired
	private MediaService mediaService;
	@Override
	public MediaModel saveFile(MultipartFile multipartFile) throws IOException {
		String extension = FilenameUtils.getExtension(multipartFile.getOriginalFilename());
		String imgName = FilenameUtils.removeExtension(multipartFile.getOriginalFilename());
		String key = FOLDER + imgName + "." + extension;
		saveImageToServer(multipartFile, key);
		MediaModel media= new MediaModel();
		media.setName(imgName);
		media.setExt(extension);
		media.setCreatedTime(new Date());
		media.setUrl("https://juvi.sgp1.digitaloceanspaces.com/files/"+imgName+"."+extension);
		return mediaService.createMedia(media);
	}

	@Override
	public void deleteFile(Long fileId) throws Exception {
		
	}
	
	private void saveImageToServer(MultipartFile multipartFile, String key) throws IOException {
		ObjectMetadata metadata = new ObjectMetadata();
		metadata.setContentLength(multipartFile.getInputStream().available());
		if (multipartFile.getContentType() != null && !"".equals(multipartFile.getContentType())) {
			metadata.setContentType(multipartFile.getContentType());
		}
		s3Client.putObject(new PutObjectRequest("juvi", key, multipartFile.getInputStream(), metadata)
				.withCannedAcl(CannedAccessControlList.PublicRead));
	}

}
