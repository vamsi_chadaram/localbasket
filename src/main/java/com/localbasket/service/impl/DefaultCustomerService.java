package com.localbasket.service.impl;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.localbasket.dao.CustomerDao;
import com.localbasket.model.CustomerModel;
import com.localbasket.model.UserModel;
import com.localbasket.service.CustomerService;
import com.localbasket.service.StoreService;
import com.localbasket.service.UserService;

@Service
public class DefaultCustomerService implements CustomerService, UserDetailsService {

	@Autowired
	private CustomerDao customerDao;

	@Autowired
	private UserService userService;

	@Autowired
	private StoreService storeService;
	
	@Override
	public CustomerModel save(CustomerModel model) {
		String referralCode= createReferralCode(model);
		model.setReferralCode(referralCode);
		return customerDao.save(model);
	}

	@Override
	public CustomerModel findById(Long id) {
		Optional<CustomerModel> customer = customerDao.findById(id);
		if (!customer.isPresent()) {

		}
		return customer.get();
	}

	@Override
	public Page<CustomerModel> findAll(Pageable paging) {
		return customerDao.findAll(paging);
	}

	@Override
	public Iterable<CustomerModel> findAll() {
		return customerDao.findAll();
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		boolean emailLogin = true;
		UserModel user = userService.findByEmail(username);
		if (null != user) {
			return new org.springframework.security.core.userdetails.User(emailLogin ? user.getEmail() : user.getMobile(),
					user.getPassword(), getAuthority(user.getUserType()));
		} else {
			emailLogin = false;
			user = userService.findByMobile(username);
		}
		
		if(null == user) {
			emailLogin = true;
			user=storeService.findByEmail(username);
		}
		
		if(null == user) {
			emailLogin = false;
			user = storeService.findByContactNumber(username);
		}
		return new org.springframework.security.core.userdetails.User(emailLogin ? user.getEmail() : user.getMobile(),
				user.getPassword(), getAuthority(user.getUserType()));
	}

	private List<SimpleGrantedAuthority> getAuthority(String userType) {
		return Arrays.asList(new SimpleGrantedAuthority("ROLE_" + userType));
	}

	@Override
	public CustomerModel findByMobile(String mobile) {
		return customerDao.findByMobile(mobile);
	}

	@Override
	public UserModel getCurrentUser() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (null != authentication && null != authentication.getPrincipal()) {
			UserDetails userDetail = (UserDetails) authentication.getPrincipal();
			String userMobile = userDetail.getUsername();
			UserModel user = userService.findByEmail(userMobile);
			if (null == user) {
				user = userService.findByMobile(userMobile);
			}
			return user;
		} else {
			return null;
		}
	}

	@Override
	public void remove(CustomerModel t) {

	}

	@Override
	public CustomerModel findByUsername(String userName) {
		return customerDao.findByEmail(userName);
	}

	@Override
	public boolean findByReferralCode(String referralCode) {
		CustomerModel customer = customerDao.findByReferralCode(referralCode);
		if (null != customer) {
			return true;
		}
		return false;
	}

	public String createReferralCode(CustomerModel customer) {

		String str1= null;
		if(null != customer.getFirstName() && customer.getFirstName().length() > 1) {
			str1 = customer.getFirstName().replaceAll("\\s","").substring(0, 2);
		} else {
			str1= customer.getFirstName();
		}
		String str2= null;
		
		if(null != customer.getLastName() && customer.getLastName().length() > 1) {
			str2 = customer.getLastName().replaceAll("\\s","").substring(0, 2);
		} else {
			str2 = customer.getLastName();
		}
		String str3 = customer.getMobile().substring(customer.getMobile().length() - 4);

		String referralCode = str1 + str2 + str3;

		if (findByReferralCode(referralCode)) {
			str3 = customer.getMobile().substring(customer.getMobile().length() - 6);
			referralCode = str1 + str2 + str3;

			if (findByReferralCode(referralCode)) {
				return "";
			} else {
				return referralCode;
			}
		}
		return referralCode;
	}
}
