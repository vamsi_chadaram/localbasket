package com.localbasket.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.localbasket.dao.DeliveryModeDao;
import com.localbasket.model.DeliveryModeModel;
import com.localbasket.service.DeliveryModeService;

@Service
public class DefaultDeliveryModeService implements DeliveryModeService{

	@Autowired
	private DeliveryModeDao deliveryModeDao;
	
	@Override
	public DeliveryModeModel createDeliveryMode(DeliveryModeModel delivertMode) {
		return deliveryModeDao.save(delivertMode);
	}
	
	@Override
	public DeliveryModeModel getDeliveryMode(String mode) {
		return deliveryModeDao.findByMode(mode);
	}
}
