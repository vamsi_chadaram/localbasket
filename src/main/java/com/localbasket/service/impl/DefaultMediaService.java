package com.localbasket.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.localbasket.dao.MediaContainerDao;
import com.localbasket.dao.MediaDao;
import com.localbasket.model.MediaContainer;
import com.localbasket.model.MediaModel;
import com.localbasket.service.MediaService;

@Service
public class DefaultMediaService implements MediaService{

	@Autowired
	private MediaDao mediaDao;
	
	@Autowired
	private MediaContainerDao mediaContainerDao;
	
	@Override
	public MediaModel createMedia(MediaModel media) {
		return mediaDao.save(media);
	}

	@Override
	public MediaContainer createMediaContainer(MediaContainer mediaContainer) {
		return mediaContainerDao.save(mediaContainer);
	}

}
