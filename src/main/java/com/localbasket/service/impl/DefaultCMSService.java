package com.localbasket.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.localbasket.dao.CMSPageDao;
import com.localbasket.dao.MediaPositionDao;
import com.localbasket.dao.PositionDao;
import com.localbasket.model.MediaPositionModel;
import com.localbasket.model.wcms.ContentPageModel;
import com.localbasket.model.wcms.PositionModel;
import com.localbasket.service.CMSService;

@Service
public class DefaultCMSService implements CMSService{

	@Autowired
	private CMSPageDao cmsPageDao;
	
	@Autowired
	private PositionDao positionDao;
	
	@Autowired
	private MediaPositionDao mediaPositionDao;
	
	@Override
	public ContentPageModel getPage(String pageCode) {
		return cmsPageDao.findByPageCode(pageCode);
	}

	@Override
	public List<PositionModel> getPositionsByPage(ContentPageModel contentPage) {
		return positionDao.findByContentPage(contentPage);
	}

	@Override
	public List<MediaPositionModel> findByPosition(PositionModel position) {
		return mediaPositionDao.findByPosition(position);
	}

	@Override
	public ContentPageModel createPage(ContentPageModel contentPage) {
		return cmsPageDao.save(contentPage);
	}

	@Override
	public PositionModel createPosition(PositionModel position) {
		return positionDao.save(position);
	}

	@Override
	public MediaPositionModel createMediaPosition(MediaPositionModel mediaPosition) {
		return mediaPositionDao.save(mediaPosition);
	}
}
