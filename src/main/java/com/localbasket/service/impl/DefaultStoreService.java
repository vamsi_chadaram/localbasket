package com.localbasket.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.localbasket.dao.RootCategoryDao;
import com.localbasket.dao.StoreAddressDao;
import com.localbasket.dao.StoreDao;
import com.localbasket.model.RootCategoryModel;
import com.localbasket.model.StoreAddressModel;
import com.localbasket.model.StoreModel;
import com.localbasket.service.StoreService;

@Service
public class DefaultStoreService implements StoreService{

	@Autowired
	private StoreDao storeDao;
	
	@Autowired
	private RootCategoryDao rootCategoryDao;
	
	@Autowired
	private StoreAddressDao storeAddressDao;
	
	@Override
	public StoreModel save(StoreModel model) {
		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
		
		if(null == model.getId()) {
			model.setPassword(encoder.encode(model.getPassword()));
		} else {
			StoreModel existingStore=findById(model.getId());
			model.setPassword(existingStore.getPassword());
		}
		model.setUserType("STORE");
		return storeDao.save(model);
	}

	@Override
	public StoreModel findById(Long id) {
		Optional<StoreModel> store=storeDao.findById(id);
		if(!store.isPresent()) {
			
		}
		return store.get();
	}

	@Override
	public Page<StoreModel> findAll(Pageable paging) {
		return storeDao.findAll(paging);
	}

	@Override
	public Iterable<StoreModel> findAll() {
		return storeDao.findAll();
	}

	@Override
	public List<StoreModel> getStoresByCategory(Long categoryId) {
		Optional<RootCategoryModel> rootCategoryModel=rootCategoryDao.findById(categoryId);
		return storeDao.findByRootCategory(rootCategoryModel.get());
	}

	@Override
	public StoreModel findByEmail(String email) {
		return storeDao.findByEmail(email);
	}

	@Override
	public StoreModel getCurrentStoreUser() {
		Authentication authentication = SecurityContextHolder.getContext()
			    .getAuthentication();
		if(null != authentication && null != authentication.getPrincipal()) {
			UserDetails userDetail=(UserDetails) authentication.getPrincipal();
			String username=userDetail.getUsername();
			return storeDao.findByEmail(username);
		} else {
			return null;
		}
	}

	@Override
	public void remove(StoreModel t) {
		
	}

	@Override
	public StoreAddressModel saveStoreAddress(StoreAddressModel storeAddress) {
		return storeAddressDao.save(storeAddress);
	}

	@Override
	public StoreModel findByContactNumber(String contactNumber) {
		return storeDao.findByContactNumber(contactNumber);
	}
}
