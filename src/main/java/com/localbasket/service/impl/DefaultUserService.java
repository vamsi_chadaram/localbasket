package com.localbasket.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.localbasket.dao.UserDao;
import com.localbasket.model.UserModel;
import com.localbasket.service.UserService;

@Service
public class DefaultUserService implements UserService{

	@Autowired
	private UserDao userDao;
	
	@Override
	public UserModel findByEmail(String username) {
		return userDao.findByEmail(username);
	}

	@Override
	public UserModel findByMobile(String mobile) {
		return userDao.findByMobile(mobile);
	}

	@Override
	public boolean findByReferralCode(String referralCode) {
		UserModel userModel=userDao.findByReferralCode(referralCode);
		
		if(null != userModel) {
			return true;
		}
		return false;
	}
}
