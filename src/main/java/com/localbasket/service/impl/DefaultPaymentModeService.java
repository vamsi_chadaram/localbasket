package com.localbasket.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.localbasket.dao.PaymentModeDao;
import com.localbasket.model.PaymentModeModel;
import com.localbasket.service.PaymentModeService;

@Service
public class DefaultPaymentModeService implements PaymentModeService{

	@Autowired
	private PaymentModeDao paymentModeDao;
	
	@Override
	public PaymentModeModel save(PaymentModeModel model) {
		return paymentModeDao.save(model);
	}

	@Override
	public PaymentModeModel findById(Long id) {
		Optional<PaymentModeModel> paymentMode= paymentModeDao.findById(id);
		return paymentMode.get();
	}

	@Override
	public Page<PaymentModeModel> findAll(Pageable paging) {
		return null;
	}

	@Override
	public List<PaymentModeModel> findAll() {
		return paymentModeDao.findAll();
	}

	@Override
	public void remove(PaymentModeModel t) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public PaymentModeModel getPaymentMode(String mode) {
		return paymentModeDao.findByMode(mode);
	}

}
