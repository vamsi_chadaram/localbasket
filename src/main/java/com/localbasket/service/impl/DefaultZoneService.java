package com.localbasket.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.localbasket.dao.ZoneDao;
import com.localbasket.model.CityModel;
import com.localbasket.model.ZoneModel;
import com.localbasket.service.ZoneService;

@Service
public class DefaultZoneService implements ZoneService{

	@Autowired
	private ZoneDao zoneDao;

	@Override
	public ZoneModel findById(Long zoneId) {
		return zoneDao.getOne(zoneId);
	}

	@Override
	public List<ZoneModel> getZonesByCity(CityModel city){
		return zoneDao.findByCity(city);
	}
}
