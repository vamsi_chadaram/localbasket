package com.localbasket.service;

import java.util.List;

import com.localbasket.model.AreaModel;
import com.localbasket.model.DeliveryPersonModel;
import com.localbasket.model.OrderModel;

public interface DeliveryPersonService extends GenericService<DeliveryPersonModel>{
	DeliveryPersonModel findByEmailAndActive(String email, boolean active);
	List<DeliveryPersonModel> findByArea(AreaModel areaModel);
	List<OrderModel> findByDeliveryPerson(DeliveryPersonModel deliveryPerson);
	DeliveryPersonModel findByEmail(String email);
}
