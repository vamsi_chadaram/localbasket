package com.localbasket.service;

import com.localbasket.model.RootCategoryModel;

public interface RootCategoryService extends GenericService<RootCategoryModel>{

}
