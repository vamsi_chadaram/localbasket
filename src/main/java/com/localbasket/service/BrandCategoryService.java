package com.localbasket.service;

import java.util.List;

import com.localbasket.model.BrandCategoryModel;
import com.localbasket.model.CategoryModel;

public interface BrandCategoryService extends GenericService<BrandCategoryModel>{
	List<BrandCategoryModel> findByCategory(CategoryModel category);
}
