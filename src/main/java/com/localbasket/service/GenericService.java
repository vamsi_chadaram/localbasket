package com.localbasket.service;


import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface GenericService<T> {
	T save(T model);
	T findById(Long id);
	Page<T> findAll(Pageable paging);
	Iterable<T> findAll();
	void remove(T t);
}
