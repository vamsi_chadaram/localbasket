package com.localbasket.service;

import java.util.List;

import com.localbasket.model.MediaPositionModel;
import com.localbasket.model.wcms.ContentPageModel;
import com.localbasket.model.wcms.PositionModel;

public interface CMSService {
	ContentPageModel createPage(ContentPageModel contentPage);
	ContentPageModel getPage(String pageCode);
	List<PositionModel> getPositionsByPage(ContentPageModel contentPage);
	List<MediaPositionModel> findByPosition(PositionModel position);
	PositionModel createPosition(PositionModel position);
	MediaPositionModel createMediaPosition(MediaPositionModel mediaPosition);
}
