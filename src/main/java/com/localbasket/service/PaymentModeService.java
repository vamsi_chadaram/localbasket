package com.localbasket.service;

import java.util.List;

import com.localbasket.model.PaymentModeModel;

public interface PaymentModeService extends GenericService<PaymentModeModel>{
	List<PaymentModeModel> findAll();
	PaymentModeModel getPaymentMode(String mode);
}
