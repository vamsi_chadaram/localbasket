package com.localbasket.service;

import java.util.List;
import com.localbasket.cart.data.CommerceCartData;
import com.localbasket.model.CartModel;
import com.localbasket.model.CustomerModel;
import com.localbasket.model.StoreModel;

public interface CartService extends GenericService<CartModel>{
	List<CartModel> findByCustomerAndStore(CustomerModel customer, StoreModel store);
	CartModel addToCart(CommerceCartData commerceCart);
	CartModel getCurrentCart();
	CartModel findByCustomer(CustomerModel customer);
}
