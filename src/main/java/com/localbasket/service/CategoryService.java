package com.localbasket.service;

import java.util.List;

import com.localbasket.exception.OrderNotFoundException;
import com.localbasket.model.CategoryModel;

public interface CategoryService extends GenericService<CategoryModel>{
	List<CategoryModel> listByRootCategory(Long rootCategoryId) throws OrderNotFoundException;
}
