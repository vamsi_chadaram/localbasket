package com.localbasket.service;

import java.util.List;

import com.localbasket.model.StoreAddressModel;
import com.localbasket.model.StoreModel;

public interface StoreService extends GenericService<StoreModel>{
	List<StoreModel> getStoresByCategory(Long categoryId);
	StoreModel findByEmail(String email);
	StoreModel getCurrentStoreUser();
	StoreAddressModel saveStoreAddress(StoreAddressModel storeAddress);
	StoreModel findByContactNumber(String contactNumber);
}
