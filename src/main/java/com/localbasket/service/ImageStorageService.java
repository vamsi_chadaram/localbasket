package com.localbasket.service;

import java.io.IOException;

import org.springframework.web.multipart.MultipartFile;
import com.localbasket.model.MediaModel;

public interface ImageStorageService {
	MediaModel saveFile(MultipartFile multipartFile) throws IOException;
	void deleteFile(Long fileId) throws Exception;
}
