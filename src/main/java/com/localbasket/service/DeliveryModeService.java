package com.localbasket.service;

import com.localbasket.model.DeliveryModeModel;

public interface DeliveryModeService {
	DeliveryModeModel createDeliveryMode(DeliveryModeModel delivertMode);
	DeliveryModeModel getDeliveryMode(String mode);
}
