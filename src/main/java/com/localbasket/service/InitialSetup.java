package com.localbasket.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.localbasket.model.CategoryModel;
import com.localbasket.model.RootCategoryModel;

@Service
public class InitialSetup {
	
	@Autowired
	private CategoryService categoryService;
	
	@Autowired
	private RootCategoryService rootCategoryService;
	
	public void createInitialData() {
		
		RootCategoryModel rootCategory= new RootCategoryModel();
		rootCategory.setId(Long.valueOf(2));
		rootCategory.setName("Grocery");
		rootCategory.setStatus(true);
		rootCategoryService.save(rootCategory);
		
		RootCategoryModel rootCategory2= new RootCategoryModel();
		rootCategory2.setId(Long.valueOf(2));
		rootCategory2.setName("Meat");
		rootCategory2.setStatus(true);
		rootCategoryService.save(rootCategory2);
		
		CategoryModel category1= new CategoryModel();
		category1.setId(Long.valueOf(2));
		category1.setName("Grocery");
		categoryService.save(category1);
		
		CategoryModel category2= new CategoryModel();
		category2.setId(Long.valueOf(3));
		category2.setName("Meat");
		categoryService.save(category2);
		
	}
}
