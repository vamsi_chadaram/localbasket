package com.localbasket.firebase.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class NavigationData {
	@JsonProperty("url")
	private String url;
	@JsonProperty("dl")
	private String dl;
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getDl() {
		return dl;
	}
	public void setDl(String dl) {
		this.dl = dl;
	}
}
