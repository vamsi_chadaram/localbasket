package com.localbasket.firebase.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class NotificationDto {
	@JsonProperty("title")
	private String title;
	@JsonProperty("body")
	private String body;
	@JsonProperty("mutable_content")
	private Boolean mutableContent;
	@JsonProperty("sound")
	private String sound;
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getBody() {
		return body;
	}
	public void setBody(String body) {
		this.body = body;
	}
	public Boolean getMutableContent() {
		return mutableContent;
	}
	public void setMutableContent(Boolean mutableContent) {
		this.mutableContent = mutableContent;
	}
	public String getSound() {
		return sound;
	}
	public void setSound(String sound) {
		this.sound = sound;
	}
}
