package com.localbasket.firebase.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PushNotificationRequest {
	@JsonProperty("to")
	private String to;
	@JsonProperty("notification")
	private NotificationDto notification;
	@JsonProperty("data")
	private NavigationData data;
	public String getTo() {
		return to;
	}
	public void setTo(String to) {
		this.to = to;
	}
	public NotificationDto getNotification() {
		return notification;
	}
	public void setNotification(NotificationDto notification) {
		this.notification = notification;
	}
	public NavigationData getData() {
		return data;
	}
	public void setData(NavigationData data) {
		this.data = data;
	}
}
