package com.localbasket.firebase.integration;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.localbasket.enumeration.OrderStatus;
import com.localbasket.firebase.dto.NavigationData;
import com.localbasket.firebase.dto.NotificationDto;
import com.localbasket.firebase.dto.PushNotificationRequest;
import com.localbasket.model.CustomerModel;
import com.localbasket.model.OrderModel;
import com.localbasket.model.StoreModel;

@Component
public class PushnotificationClient {
	
	public void sendOrderNotification(OrderModel order) {
		NotificationDto notificationDto= new NotificationDto();
		notificationDto.setTitle("Order "+order.getId()+"has been placed");
		notificationDto.setBody(order.getOrderEntries().size()+"Products placed with total "+order.getTotal());
		notificationDto.setMutableContent(Boolean.TRUE);
		notificationDto.setSound("my_sound");
		
		NavigationData navigationData= new NavigationData();
		navigationData.setDl("");
		navigationData.setUrl("");
		
		StoreModel store=order.getStore();
		PushNotificationRequest pnr= new PushNotificationRequest();
		pnr.setData(navigationData);
		pnr.setNotification(notificationDto);
		pnr.setTo(store.getFcmToken());
		
		RestTemplate restTemplate= new RestTemplate();
		
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", "key=AAAAVrNSLjk:APA91bFXzoOVHgX2x8Y0wLWwZThxrbvdfPmE4bgfnU6zxaa9XtaraXtn25iYvromxfukZLCOHeJ7dXkc7UbDMP93lnvQK3V6d8grm-4n1jBvqsEH33HCkNA4_yPMzxSzpxv8wRbHa8sY");
		headers.setContentType(MediaType.APPLICATION_JSON);
		
		HttpEntity<PushNotificationRequest> requestEntity = 
			     new HttpEntity<>(pnr, headers);
		
		ResponseEntity<String> response = restTemplate.exchange("https://fcm.googleapis.com/fcm/send", HttpMethod.POST, requestEntity, 
	              String.class);
		
		if(response.getStatusCode() == HttpStatus.ACCEPTED) {
			//TODO
		}
	}
	
	public void sendOrderConfirmation(OrderModel order) {

		NotificationDto notificationDto= new NotificationDto();
		
		if(order.getOrderStatus().equals(OrderStatus.CONFIRMED)) {
			notificationDto.setTitle("Order confirmed");
			notificationDto.setBody(order.getOrderEntries().size()+" confirmed by Store "+order.getStore().getStoreName());
		}
		
		if(order.getOrderStatus().equals(OrderStatus.OUT_FOR_DELIVERY)) {
			notificationDto.setTitle("Your order out for delivery");
			notificationDto.setBody("You order with " +order.getOrderEntries().size()+" Quantity out for delivery ");
		}
		
		if(order.getOrderStatus().equals(OrderStatus.DELIVERED)) {
			notificationDto.setTitle("Your order has been delivered");
			notificationDto.setBody("You order with " +order.getOrderEntries().size()+" has been delivered");
		}
		notificationDto.setMutableContent(Boolean.TRUE);
		notificationDto.setSound("my_sound");
		
		NavigationData navigationData= new NavigationData();
		navigationData.setDl("");
		navigationData.setUrl("");
		
		CustomerModel store=order.getCustomer();
		PushNotificationRequest pnr= new PushNotificationRequest();
		pnr.setData(navigationData);
		pnr.setNotification(notificationDto);
		pnr.setTo(store.getFcmToken());
		
		RestTemplate restTemplate= new RestTemplate();
		
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", "key=AAAAVrNSLjk:APA91bFXzoOVHgX2x8Y0wLWwZThxrbvdfPmE4bgfnU6zxaa9XtaraXtn25iYvromxfukZLCOHeJ7dXkc7UbDMP93lnvQK3V6d8grm-4n1jBvqsEH33HCkNA4_yPMzxSzpxv8wRbHa8sY");
		headers.setContentType(MediaType.APPLICATION_JSON);
		
		HttpEntity<PushNotificationRequest> requestEntity = 
			     new HttpEntity<>(pnr, headers);
		
		ResponseEntity<String> response = restTemplate.exchange("https://fcm.googleapis.com/fcm/send", HttpMethod.POST, requestEntity, 
	              String.class);
		
		if(response.getStatusCode() == HttpStatus.ACCEPTED) {
			//TODO
		}
	
	}
}
