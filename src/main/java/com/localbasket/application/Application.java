package com.localbasket.application;

import java.io.IOException;
import java.util.Properties;

import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.exception.VelocityException;
import org.modelmapper.ModelMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.context.event.ApplicationEventMulticaster;
import org.springframework.context.event.SimpleApplicationEventMulticaster;
import org.springframework.core.task.SimpleAsyncTaskExecutor;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.scheduling.support.TaskUtils;
import org.springframework.ui.velocity.VelocityEngineFactory;

@SpringBootApplication(scanBasePackages = {"com.localbasket"})
@EnableJpaRepositories("com.localbasket.dao")
@EntityScan("com.localbasket.model")
public class Application {

    public static void main(String[] args) {
    	System.out.println("Local Basket");
        SpringApplication.run(Application.class, args);
    }
    
    @Bean
    public JavaMailSender getMailSender() {
        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
 
        mailSender.setHost("smtp.hostinger.in");
        mailSender.setPort(587);
        mailSender.setUsername("sales@localbaskethd.com");
        mailSender.setPassword("LocalBasket@2021");
 
        Properties javaMailProperties = new Properties();
        javaMailProperties.put("mail.smtp.starttls.enable", "true");
        javaMailProperties.put("mail.smtp.auth", "true");
        javaMailProperties.put("mail.transport.protocol", "smtp");
        javaMailProperties.put("mail.debug", "true");
 
        mailSender.setJavaMailProperties(javaMailProperties);
        return mailSender;
    }
    
    @Bean
    public VelocityEngine getVelocityEngine() throws VelocityException, IOException {
        VelocityEngineFactory velocityEngineFactory = new VelocityEngineFactory();
        Properties props = new Properties();
        props.put("resource.loader", "class");
        props.put("class.resource.loader.class", "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
 
        velocityEngineFactory.setVelocityProperties(props);
        return velocityEngineFactory.createVelocityEngine();
    }
    
    @Bean
    @Scope("singleton")
    public ApplicationEventMulticaster eventMulticaster() {
      SimpleApplicationEventMulticaster eventMulticaster 
                                              = new SimpleApplicationEventMulticaster();
      eventMulticaster.setTaskExecutor(new SimpleAsyncTaskExecutor());
      eventMulticaster.setErrorHandler(TaskUtils.LOG_AND_PROPAGATE_ERROR_HANDLER);
      return eventMulticaster;
    }
    
    @Bean
   	public ModelMapper modelMapper() {
   	    return new ModelMapper();
   	}
}
