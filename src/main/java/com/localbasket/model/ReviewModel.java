package com.localbasket.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="REVIEW")
public class ReviewModel {
	
	@Id
	@GeneratedValue
	private Long id;
	
	@Column(name="COMMENT")
	private String comment;
	
	@Column(name="RATING")
	private Integer rating;
	
	@OneToOne
	@JoinColumn(name="STORE")
	private StoreModel store;
	
	@OneToOne
	@JoinColumn(name="CUSTOMER")
	private CustomerModel customer;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public Integer getRating() {
		return rating;
	}

	public void setRating(Integer rating) {
		this.rating = rating;
	}

	public StoreModel getStore() {
		return store;
	}

	public void setStore(StoreModel store) {
		this.store = store;
	}

	public CustomerModel getCustomer() {
		return customer;
	}

	public void setCustomer(CustomerModel customer) {
		this.customer = customer;
	}
}
