package com.localbasket.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="PRODUCT")
public class ProductModel {
	
	@Id
	@GeneratedValue
	private Long id;
	
	@Column
	private String code;
	
	@Column
	@NotBlank
	private String name;

	@Column(name="NATIVE_NAME")
	private String nativeName;
	
	@Column(name="SUMMARY")
	private String summary;
	
	@Column(name="NATIVE_SUMMARY")
	private String nativeSummary;
	
	@Column
	@NotBlank
	private String description;
	
	@Column(name="NATIVE_DESCRIPTION")
	private String nativeDescription;
	
	@OneToOne
	@JoinColumn(name="CATEGORY")
	private CategoryModel category;
	
	@OneToOne
	@JoinColumn(name="STORE")
	@JsonIgnore
	private StoreModel store;
	
	@OneToOne
	@JoinColumn(name="STOCK_LEVEL")
	private StockLevelModel stockLevel;
	
	@OneToOne
	@JoinColumn(name="PRICE_ROW")
	private PriceRowModel priceRow;
	
	@OneToOne
	@JoinColumn(name="BRAND_CATEGORY")
	private BrandCategoryModel brandCategory;
	
	@Column
	private boolean status;

	@Column
	private boolean popular;
	
	@Column
	private boolean popularCount;
	
	@OneToOne
	@JoinColumn(name="media")
	private MediaModel media;

	@Column
	private String imageUrl;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public CategoryModel getCategory() {
		return category;
	}

	public void setCategory(CategoryModel category) {
		this.category = category;
	}

	public StoreModel getStore() {
		return store;
	}

	public void setStore(StoreModel store) {
		this.store = store;
	}

	public StockLevelModel getStockLevel() {
		return stockLevel;
	}

	public void setStockLevel(StockLevelModel stockLevel) {
		this.stockLevel = stockLevel;
	}

	public PriceRowModel getPriceRow() {
		return priceRow;
	}

	public void setPriceRow(PriceRowModel priceRow) {
		this.priceRow = priceRow;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public String getNativeSummary() {
		return nativeSummary;
	}

	public void setNativeSummary(String nativeSummary) {
		this.nativeSummary = nativeSummary;
	}

	public String getNativeDescription() {
		return nativeDescription;
	}

	public void setNativeDescription(String nativeDescription) {
		this.nativeDescription = nativeDescription;
	}

	public boolean isPopular() {
		return popular;
	}

	public void setPopular(boolean popular) {
		this.popular = popular;
	}

	public boolean isPopularCount() {
		return popularCount;
	}

	public void setPopularCount(boolean popularCount) {
		this.popularCount = popularCount;
	}

	public BrandCategoryModel getBrandCategory() {
		return brandCategory;
	}

	public void setBrandCategory(BrandCategoryModel brandCategory) {
		this.brandCategory = brandCategory;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public String getNativeName() {
		return nativeName;
	}

	public void setNativeName(String nativeName) {
		this.nativeName = nativeName;
	}

	public MediaModel getMedia() {
		return media;
	}

	public void setMedia(MediaModel media) {
		this.media = media;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
}
