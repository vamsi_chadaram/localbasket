package com.localbasket.model;

import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import com.localbasket.enumeration.OrderStatus;

@Entity
@Table(name="LB_ORDER")
public class OrderModel {
	
	@Id
	@GeneratedValue
	private Long id;
	
	@Column
	private String orderCode;
	
	@Column
	private Double subTotal;
	
	@Column
	private Double deliveryCharges;
	
	@Column
	private Double total;
	
	@Column
	private Double discount;
	
	@OneToOne
	@JoinColumn(name="CUSTOMER")
	private CustomerModel customer;
	
	@OneToOne
	@JoinColumn(name="STORE")
	private StoreModel store;

	@Enumerated(EnumType.ORDINAL)
	private OrderStatus orderStatus;
	
	@OneToMany(fetch = FetchType.LAZY, targetEntity = AbstractOrderEntryModel.class, cascade = CascadeType.ALL)
	@JoinColumn(name="ORDER_ID", referencedColumnName = "id")
	private Set<AbstractOrderEntryModel> orderEntries;
	
	@OneToOne
	@JoinColumn(name="PAYMENT_MODE")
	private PaymentModeModel paymentMode;
	
	@OneToOne
	@JoinColumn(name="DELIVERY_PERSON")
	private DeliveryPersonModel deliveryPerson;
	
	@Column(name="CREATED_DATE")
	private Date createdDate;
	
	@OneToOne
	@JoinColumn(name="DELIVERY_ADDRESS")
	private AddressModel deliveryAddress;
	
	@OneToOne
	@JoinColumn(name="PAYMENT_ADDRESS")
	private AddressModel paymentAddress;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getOrderCode() {
		return orderCode;
	}

	public void setOrderCode(String orderCode) {
		this.orderCode = orderCode;
	}

	public StoreModel getStore() {
		return store;
	}

	public void setStore(StoreModel store) {
		this.store = store;
	}

	public CustomerModel getCustomer() {
		return customer;
	}

	public void setCustomer(CustomerModel customer) {
		this.customer = customer;
	}

	public OrderStatus getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(OrderStatus orderStatus) {
		this.orderStatus = orderStatus;
	}

	public Set<AbstractOrderEntryModel> getOrderEntries() {
		return orderEntries;
	}

	public void setOrderEntries(Set<AbstractOrderEntryModel> orderEntries) {
		this.orderEntries = orderEntries;
	}

	public Double getSubTotal() {
		return subTotal;
	}

	public void setSubTotal(Double subTotal) {
		this.subTotal = subTotal;
	}

	public Double getDeliveryCharges() {
		return deliveryCharges;
	}

	public void setDeliveryCharges(Double deliveryCharges) {
		this.deliveryCharges = deliveryCharges;
	}

	public Double getTotal() {
		return total;
	}

	public void setTotal(Double total) {
		this.total = total;
	}

	public Double getDiscount() {
		return discount;
	}

	public void setDiscount(Double discount) {
		this.discount = discount;
	}

	public PaymentModeModel getPaymentMode() {
		return paymentMode;
	}

	public void setPaymentMode(PaymentModeModel paymentMode) {
		this.paymentMode = paymentMode;
	}

	public DeliveryPersonModel getDeliveryPerson() {
		return deliveryPerson;
	}

	public void setDeliveryPerson(DeliveryPersonModel deliveryPerson) {
		this.deliveryPerson = deliveryPerson;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public AddressModel getDeliveryAddress() {
		return deliveryAddress;
	}

	public void setDeliveryAddress(AddressModel deliveryAddress) {
		this.deliveryAddress = deliveryAddress;
	}

	public AddressModel getPaymentAddress() {
		return paymentAddress;
	}

	public void setPaymentAddress(AddressModel paymentAddress) {
		this.paymentAddress = paymentAddress;
	}
}
