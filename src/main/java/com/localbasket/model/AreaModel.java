package com.localbasket.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="AREA")
public class AreaModel {
	
	@Id
	@GeneratedValue
	private Long id;
	
	@Column(name="AREA_NAME")
	private String name;

	@OneToOne
	@JoinColumn(name="CITY")
	private CityModel city;
	
	@Column(name="AREA_CATEGORY")
	private Integer areaCategory;
	
	@OneToOne
	@JoinColumn(name="ZONE")
	private ZoneModel zone;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public CityModel getCity() {
		return city;
	}

	public void setCity(CityModel city) {
		this.city = city;
	}

	public Integer getAreaCategory() {
		return areaCategory;
	}

	public void setAreaCategory(Integer areaCategory) {
		this.areaCategory = areaCategory;
	}

	public ZoneModel getZone() {
		return zone;
	}

	public void setZone(ZoneModel zone) {
		this.zone = zone;
	}
}
