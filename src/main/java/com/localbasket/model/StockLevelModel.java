package com.localbasket.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="STOCK_LEVEL")
public class StockLevelModel {
	
	@Id
	@GeneratedValue
	private Long id;
	
	@OneToOne
	@JoinColumn
	private ProductModel product;
	
	@Column
	private boolean forceAvailable;
	
	@Column
	private Integer availableCount;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ProductModel getProduct() {
		return product;
	}

	public void setProduct(ProductModel product) {
		this.product = product;
	}

	public boolean isForceAvailable() {
		return forceAvailable;
	}

	public void setForceAvailable(boolean forceAvailable) {
		this.forceAvailable = forceAvailable;
	}

	public Integer getAvailableCount() {
		return availableCount;
	}

	public void setAvailableCount(Integer availableCount) {
		this.availableCount = availableCount;
	}
}
