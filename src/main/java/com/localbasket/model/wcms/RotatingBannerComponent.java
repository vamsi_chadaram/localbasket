package com.localbasket.model.wcms;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import com.localbasket.model.MediaModel;

@Entity
@Table(name="ROTATING_BANNER")
public class RotatingBannerComponent {
	
	@Id
	@GeneratedValue
	private Long id;
	
	@Column(name="NAME")
	private String name;
	
	@Column(name="CODE")
	private String code;
	
	@OneToMany(fetch = FetchType.LAZY, targetEntity = MediaModel.class, cascade = CascadeType.ALL)
	@JoinColumn(name="MEDIA", referencedColumnName = "id")
	private Set<MediaModel> media;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Set<MediaModel> getMedia() {
		return media;
	}

	public void setMedia(Set<MediaModel> media) {
		this.media = media;
	}
}
