package com.localbasket.model.wcms;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="PAGE_TEMPLATE")
public class PageTemplateModel {

	@Id
	@GeneratedValue
	private Long id;
	
	@Column(name="TEMPLATE_CODE")
	private String templateCode;
	
	
}
