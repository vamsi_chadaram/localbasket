package com.localbasket.model.wcms;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="CONTENT_PAGE")
public class ContentPageModel {
	
	@Id
	@GeneratedValue
	private Long id;
	
	@Column(name="PAGE_CODE")
	private String pageCode;

	@OneToOne
	@JoinColumn(name="PAGE_TEMPLATE")
	private PageTemplateModel pageTemlate;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPageCode() {
		return pageCode;
	}

	public void setPageCode(String pageCode) {
		this.pageCode = pageCode;
	}

	public PageTemplateModel getPageTemlate() {
		return pageTemlate;
	}

	public void setPageTemlate(PageTemplateModel pageTemlate) {
		this.pageTemlate = pageTemlate;
	}
}
