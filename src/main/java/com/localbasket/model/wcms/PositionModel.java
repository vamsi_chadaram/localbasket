package com.localbasket.model.wcms;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="POSITION")
public class PositionModel {

	@Id
	@GeneratedValue
	private Long id;
	
	@Column(name="POSITION")
	private String position;
	
	@OneToOne
	@JoinColumn(name="PAGE_TEMPLATE")
	private PageTemplateModel pageTemplate;
	
	@OneToOne
	@JoinColumn(name="CONTENT_PAGE")
	private ContentPageModel contentPage;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public PageTemplateModel getPageTemplate() {
		return pageTemplate;
	}

	public void setPageTemplate(PageTemplateModel pageTemplate) {
		this.pageTemplate = pageTemplate;
	}

	public ContentPageModel getContentPage() {
		return contentPage;
	}

	public void setContentPage(ContentPageModel contentPage) {
		this.contentPage = contentPage;
	}
}
