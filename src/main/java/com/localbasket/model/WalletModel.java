package com.localbasket.model;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="WALLET")
public class WalletModel {

	@Id
	@GeneratedValue
	private Long id;
	
	@OneToOne
	@GeneratedValue
	private CustomerModel customer;
	
	@OneToMany(fetch = FetchType.LAZY, targetEntity = AbstractOrderEntryModel.class, cascade = CascadeType.ALL)
	@JoinColumn(name="AMOUNT", referencedColumnName = "id")
	private List<AmountModel> amount;

	@Column(name="TOTAL_AMOUNT")
	private BigDecimal toalWalletAmount;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public CustomerModel getCustomer() {
		return customer;
	}

	public void setCustomer(CustomerModel customer) {
		this.customer = customer;
	}

	public List<AmountModel> getAmount() {
		return amount;
	}

	public void setAmount(List<AmountModel> amount) {
		this.amount = amount;
	}

	public BigDecimal getToalWalletAmount() {
		return toalWalletAmount;
	}

	public void setToalWalletAmount(BigDecimal toalWalletAmount) {
		this.toalWalletAmount = toalWalletAmount;
	}
	
}
