package com.localbasket.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

@Entity
@Table(name="STORE")
public class StoreModel extends UserModel{
	
	@Column
	@NotBlank
	private String storeName;
	
	@Column
	private String storeCode;
	
	@Column
	private boolean status;
	
	@Column
	@NotBlank
	private String contactNumber;
	
	@OneToOne
	@JoinColumn(name="CATEGORY")
	private RootCategoryModel rootCategory;
	
	@Column(name="AVAILABLE_FROM")
	private int availableFrom;
	
	@Column(name="AVAILABLE_TO")
	private int availableTo;
	
	@Column(name="AVG_RATING")
	private Double avgRating;
	
	@Column(name="TOTAL_REVIEWS")
	private Long totalReviews;
	
	@Column(name="CONTAIN_PRODUCTS")
	private Boolean containProducts;
	
	@OneToOne
	@JoinColumn(name="DELIVERY_MODE")
	private DeliveryModeModel deliveryMode;
	
	@OneToOne
	@JoinColumn(name="MEDIA_CONTAINER")
	private MediaContainer mediaContainer;
	
	public String getStoreName() {
		return storeName;
	}

	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}

	public String getStoreCode() {
		return storeCode;
	}

	public void setStoreCode(String storeCode) {
		this.storeCode = storeCode;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public String getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	public RootCategoryModel getRootCategory() {
		return rootCategory;
	}

	public void setRootCategory(RootCategoryModel rootCategory) {
		this.rootCategory = rootCategory;
	}

	public int getAvailableFrom() {
		return availableFrom;
	}

	public void setAvailableFrom(int availableFrom) {
		this.availableFrom = availableFrom;
	}

	public int getAvailableTo() {
		return availableTo;
	}

	public void setAvailableTo(int availableTo) {
		this.availableTo = availableTo;
	}

	public Double getAvgRating() {
		return avgRating;
	}

	public void setAvgRating(Double avgRating) {
		this.avgRating = avgRating;
	}

	public Long getTotalReviews() {
		return totalReviews;
	}

	public void setTotalReviews(Long totalReviews) {
		this.totalReviews = totalReviews;
	}

	public Boolean getContainProducts() {
		return containProducts;
	}

	public void setContainProducts(Boolean containProducts) {
		this.containProducts = containProducts;
	}

	public DeliveryModeModel getDeliveryMode() {
		return deliveryMode;
	}

	public void setDeliveryMode(DeliveryModeModel deliveryMode) {
		this.deliveryMode = deliveryMode;
	}
	
	public MediaContainer getMediaContainer() {
		return mediaContainer;
	}

	public void setMediaContainer(MediaContainer mediaContainer) {
		this.mediaContainer = mediaContainer;
	}
}
