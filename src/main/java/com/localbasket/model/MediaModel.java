package com.localbasket.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="MEDIA")
public class MediaModel {
	@Id
	@GeneratedValue
	private Long id;
	
	@Column(name="name")
	private String name;
	
	@Column(name="ext")
	private String ext;
	
	@Column(name="createdTime")
	private Date createdTime;
	
	@Column(name="url")
	private String url;
	
	@Column(name="TYPE")
	private String type;
	
	@OneToOne
	@JoinColumn(name="ROOT_CATEGORY")
	private RootCategoryModel rootCategory;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getExt() {
		return ext;
	}
	public void setExt(String ext) {
		this.ext = ext;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Date getCreatedTime() {
		return createdTime;
	}
	public void setCreatedTime(Date createdTime) {
		this.createdTime = createdTime;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public RootCategoryModel getRootCategory() {
		return rootCategory;
	}
	public void setRootCategory(RootCategoryModel rootCategory) {
		this.rootCategory = rootCategory;
	}
}
