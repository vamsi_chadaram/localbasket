package com.localbasket.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="CATEGORY")
public class CategoryModel {
	
	@Id
	@GeneratedValue
	private Long id;

	@Column
	private String name;

	@OneToOne
	@JoinColumn(name="ROOT_CATEGORY")
	private RootCategoryModel rootCategory;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public RootCategoryModel getRootCategory() {
		return rootCategory;
	}

	public void setRootCategory(RootCategoryModel rootCategory) {
		this.rootCategory = rootCategory;
	}
	
}
