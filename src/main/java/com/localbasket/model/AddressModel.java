package com.localbasket.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Entity
@Table(name="ADDRESS")
public class AddressModel {
	
	@Id
	@GeneratedValue
	private Long id;
	
	@Column(name="LINE_1")
	@NotBlank
	private String line1;
	
	@Column(name="LINE_2")
	@NotBlank
	private String line2;
	
	@Column(name="LINE_3")
	private String line3;
	
	@Column(name="MOBILE")
	@NotNull
	private String mobile;
	
	@Column(name="PINCODE")
	private String pincode;
	
	@Column(name="DEFAULT_ADDRESS")
	private boolean defaultAddress;
	
	@OneToOne
	@JoinColumn(name="AREA")
	@NotNull
	private AreaModel area;
	
	@Column(name="LATITUDE")
	private String latitude;
	
	@Column(name="LANGITUDE")
	private String langitude;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getLine1() {
		return line1;
	}
	public void setLine1(String line1) {
		this.line1 = line1;
	}
	public String getLine2() {
		return line2;
	}
	public void setLine2(String line2) {
		this.line2 = line2;
	}
	public String getLine3() {
		return line3;
	}
	public void setLine3(String line3) {
		this.line3 = line3;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getPincode() {
		return pincode;
	}
	public void setPincode(String pincode) {
		this.pincode = pincode;
	}
	public boolean isDefaultAddress() {
		return defaultAddress;
	}
	public void setDefaultAddress(boolean defaultAddress) {
		this.defaultAddress = defaultAddress;
	}
	public AreaModel getArea() {
		return area;
	}
	public void setArea(AreaModel area) {
		this.area = area;
	}
	public String getLatitude() {
		return latitude;
	}
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	public String getLangitude() {
		return langitude;
	}
	public void setLangitude(String langitude) {
		this.langitude = langitude;
	}
	
}
