package com.localbasket.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="STORE_ADDRESS")
public class StoreAddressModel {
	@Id
	@GeneratedValue
	private Long id;
	
	@OneToOne
	@JoinColumn(name="STORE_ADDRESS")
	private AddressModel address;
	
	@OneToOne
	@JoinColumn(name="STORE")
	private StoreModel store;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public AddressModel getAddress() {
		return address;
	}

	public void setAddress(AddressModel address) {
		this.address = address;
	}

	public StoreModel getStore() {
		return store;
	}

	public void setStore(StoreModel store) {
		this.store = store;
	}
	
}
