package com.localbasket.model;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.localbasket.enumeration.OrderStatus;

@Entity
@Table(name="CART")
public class CartModel implements Cloneable{
	
	@Id
	@GeneratedValue
	private Long id;
	
	@Column
	private Integer totalQuantity;
	
	@Column
	private Double subTotal;
	
	@Column
	private Double deliveryCharges;
	
	@Column
	private String deliveryText;
	
	@Column
	private Double total;
	
	@Column
	private Double discount;

	@OneToOne
	@JoinColumn(name="DELIVERY_ADDRESS")
	private AddressModel deliveryAddress;
	
	@OneToOne
	@JoinColumn(name="PAYMENT_ADDRESS")
	private AddressModel paymentAddress;
	
	@OneToOne
	@JoinColumn(name="CUSTOMER")
	private CustomerModel customer;
	
	@OneToOne
	@JoinColumn(name="STORE")
	private StoreModel store;

	@Enumerated(EnumType.ORDINAL)
	private OrderStatus orderStatus;
	
	@OneToOne
	@JoinColumn(name="PAYMENT_MODE")
	private PaymentModeModel paymentMode;
	
	@Column(name="CREATED_DATE")
	private Date createdDate;
	
	public Double getSubTotal() {
		return subTotal;
	}

	public void setSubTotal(Double subTotal) {
		this.subTotal = subTotal;
	}

	public Double getDeliveryCharges() {
		return deliveryCharges;
	}

	public void setDeliveryCharges(Double deliveryCharges) {
		this.deliveryCharges = deliveryCharges;
	}

	public Double getTotal() {
		return total;
	}

	public void setTotal(Double total) {
		this.total = total;
	}

	public Double getDiscount() {
		return discount;
	}

	public void setDiscount(Double discount) {
		this.discount = discount;
	}

	public AddressModel getDeliveryAddress() {
		return deliveryAddress;
	}

	public void setDeliveryAddress(AddressModel deliveryAddress) {
		this.deliveryAddress = deliveryAddress;
	}

	public AddressModel getPaymentAddress() {
		return paymentAddress;
	}

	public void setPaymentAddress(AddressModel paymentAddress) {
		this.paymentAddress = paymentAddress;
	}

	public CustomerModel getCustomer() {
		return customer;
	}

	public void setCustomer(CustomerModel customer) {
		this.customer = customer;
	}

	public StoreModel getStore() {
		return store;
	}

	public void setStore(StoreModel store) {
		this.store = store;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public OrderStatus getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(OrderStatus orderStatus) {
		this.orderStatus = orderStatus;
	}

	public PaymentModeModel getPaymentMode() {
		return paymentMode;
	}

	public void setPaymentMode(PaymentModeModel paymentMode) {
		this.paymentMode = paymentMode;
	}
	
	public Object clone()throws CloneNotSupportedException{  
		return super.clone();  
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Integer getTotalQuantity() {
		return totalQuantity;
	}

	public void setTotalQuantity(Integer totalQuantity) {
		this.totalQuantity = totalQuantity;
	}

	public String getDeliveryText() {
		return deliveryText;
	}

	public void setDeliveryText(String deliveryText) {
		this.deliveryText = deliveryText;
	}
	
}
