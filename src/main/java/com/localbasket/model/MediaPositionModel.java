package com.localbasket.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.localbasket.model.wcms.PositionModel;

@Entity
@Table(name="MEDIA_POSITION")
public class MediaPositionModel {
	
	@Id
	@GeneratedValue
	private Long id;
	
	@OneToOne
	@JoinColumn(name="MEDIA")
	private MediaModel media;
	
	@OneToOne
	@JoinColumn(name="POSITION")
	private PositionModel position;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public MediaModel getMedia() {
		return media;
	}

	public void setMedia(MediaModel media) {
		this.media = media;
	}

	public PositionModel getPosition() {
		return position;
	}

	public void setPosition(PositionModel position) {
		this.position = position;
	}
}
