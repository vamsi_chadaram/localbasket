package com.localbasket.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="DELIVERY_PERSON")
public class DeliveryPersonModel extends UserModel{

	@Column(name="AVAILABLE")
	private boolean available;

	@Column(name="ACTIVE")
	private boolean active;
	
	@OneToOne
	@JoinColumn(name="AREA")
	private AreaModel area;
	
	public boolean isAvailable() {
		return available;
	}

	public void setAvailable(boolean available) {
		this.available = available;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public AreaModel getArea() {
		return area;
	}

	public void setArea(AreaModel area) {
		this.area = area;
	}
}
