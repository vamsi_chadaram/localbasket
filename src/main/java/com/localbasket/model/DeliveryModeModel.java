package com.localbasket.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="DELIVERY_MODE")
public class DeliveryModeModel {
	
	@Id
	@GeneratedValue
	private Long id;
	
	@Column(name="MODE")
	private String mode;
	
	@Column(name="CHARGES")
	private Double charges;
	
	@Column(name="TYPE")
	private String type;

	@Column(name="CONDITION")
	private String condition;
	
	@Column(name="threshold")
	private Integer threshold;
	
	@Column(name="deliveryText")
	private String deliveryText;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getMode() {
		return mode;
	}

	public void setMode(String mode) {
		this.mode = mode;
	}

	public Double getCharges() {
		return charges;
	}

	public void setCharges(Double charges) {
		this.charges = charges;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getCondition() {
		return condition;
	}

	public void setCondition(String condition) {
		this.condition = condition;
	}

	public Integer getThreshold() {
		return threshold;
	}

	public void setThreshold(Integer threshold) {
		this.threshold = threshold;
	}

	public String getDeliveryText() {
		return deliveryText;
	}

	public void setDeliveryText(String deliveryText) {
		this.deliveryText = deliveryText;
	}
}
