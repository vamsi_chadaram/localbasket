package com.localbasket.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="MEDIA_CONTAINER")
public class MediaContainer {

	@Id
	@GeneratedValue
	private Long id;
	
	@OneToOne
	@JoinColumn(name="THUMB_NAIL")
	private MediaModel thumbNail;
	
	@OneToOne
	@JoinColumn(name="BANNER_MEDIA")
	private MediaModel bannerMedia;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public MediaModel getThumbNail() {
		return thumbNail;
	}

	public void setThumbNail(MediaModel thumbNail) {
		this.thumbNail = thumbNail;
	}

	public MediaModel getBannerMedia() {
		return bannerMedia;
	}

	public void setBannerMedia(MediaModel bannerMedia) {
		this.bannerMedia = bannerMedia;
	}
}
