package com.localbasket.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.localbasket.model.wcms.ContentPageModel;

@Repository
public interface CMSPageDao extends JpaRepository<ContentPageModel, Long>{
	ContentPageModel findByPageCode(String pageCode);
}
