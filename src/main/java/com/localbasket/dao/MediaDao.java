package com.localbasket.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.localbasket.model.MediaModel;

@Repository
public interface MediaDao extends CrudRepository<MediaModel, Long>{
}
