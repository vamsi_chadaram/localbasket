package com.localbasket.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.localbasket.enumeration.OrderStatus;
import com.localbasket.model.CustomerModel;
import com.localbasket.model.DeliveryPersonModel;
import com.localbasket.model.OrderModel;
import com.localbasket.model.StoreModel;

@Repository
public interface OrderDao extends PagingAndSortingRepository<OrderModel, Long>{
	List<OrderModel> findByStore(StoreModel store);
	List<OrderModel> findByDeliveryPerson(DeliveryPersonModel deliveryPerson);
	List<OrderModel> findOrdersByStoreAndOrderStatus(StoreModel store, OrderStatus orderStatus);
	List<OrderModel> findByCustomer(CustomerModel customer);
	
	@Query(value = "SELECT o FROM OrderModel o WHERE o.createdDate >= :startDate AND o.createdDate <= :endDate")
	List<OrderModel> getAllBetweenDates(Date startDate, Date endDate);
	
	List<OrderModel> findByDeliveryPersonAndOrderStatus(DeliveryPersonModel deliveryPerson, OrderStatus orderStatus);
	
	@Query(value = "SELECT order FROM OrderModel order WHERE order.deliveryPerson = :deliveryPerson and order.createdDate >= :startDate AND order.createdDate <= :endDate")
	List<OrderModel> findByDeliveryPerson(DeliveryPersonModel deliveryPerson, Date startDate, Date endDate);
	
	@Query(value = "SELECT order FROM OrderModel order WHERE order.deliveryPerson = :deliveryPerson and order.createdDate >= :startDate AND order.createdDate <= :endDate and order.orderStatus = :orderStatus")
	List<OrderModel> pendingOrdersByDeliveryPerson(DeliveryPersonModel deliveryPerson, Date startDate, Date endDate, OrderStatus orderStatus);
}
