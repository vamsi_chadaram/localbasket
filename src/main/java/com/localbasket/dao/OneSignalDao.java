package com.localbasket.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.localbasket.model.OneSignalModel;

@Repository
public interface OneSignalDao extends JpaRepository<OneSignalModel, Long>{
}
