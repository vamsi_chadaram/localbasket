package com.localbasket.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.localbasket.model.MediaContainer;

@Repository
public interface MediaContainerDao extends JpaRepository<MediaContainer, Long>{
}
