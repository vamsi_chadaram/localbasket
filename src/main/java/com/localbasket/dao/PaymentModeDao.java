package com.localbasket.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.localbasket.model.PaymentModeModel;

@Repository
public interface PaymentModeDao extends CrudRepository<PaymentModeModel, Long>{
	List<PaymentModeModel> findAll();
	PaymentModeModel findByMode(String mode);
}
