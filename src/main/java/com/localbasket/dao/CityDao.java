package com.localbasket.dao;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.localbasket.model.CityModel;

@Repository
public interface CityDao extends PagingAndSortingRepository<CityModel, Long>{
	List<CityModel> findAll();
}
