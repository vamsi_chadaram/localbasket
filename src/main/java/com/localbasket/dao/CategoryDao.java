package com.localbasket.dao;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.localbasket.model.CategoryModel;
import com.localbasket.model.RootCategoryModel;

@Repository
public interface CategoryDao extends PagingAndSortingRepository<CategoryModel, Long>{
	List<CategoryModel> findByRootCategory(RootCategoryModel rootCategory);
}
