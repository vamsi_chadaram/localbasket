package com.localbasket.dao;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.localbasket.model.RootCategoryModel;
import com.localbasket.model.StoreModel;

public interface StoreDao extends PagingAndSortingRepository<StoreModel, Long>{
	List<StoreModel> findByRootCategory(RootCategoryModel rootCategory);
	StoreModel findByEmail(String email);
	StoreModel findByContactNumber(String contactNumber);
}
