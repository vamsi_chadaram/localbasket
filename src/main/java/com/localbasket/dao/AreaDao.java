package com.localbasket.dao;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.localbasket.model.AreaModel;
import com.localbasket.model.CityModel;

@Repository
public interface AreaDao extends PagingAndSortingRepository<AreaModel, Long>{
	List<AreaModel> findByCity(CityModel city);
}
