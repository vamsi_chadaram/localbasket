package com.localbasket.dao;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.localbasket.model.CartModel;
import com.localbasket.model.CustomerModel;
import com.localbasket.model.StoreModel;

public interface CartDao extends PagingAndSortingRepository<CartModel, Long>{
	List<CartModel> findByCustomerAndStore(CustomerModel customer, StoreModel store);
	CartModel findByCustomer(CustomerModel customer);
}
