package com.localbasket.dao;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.localbasket.model.CustomerModel;

public interface CustomerDao extends PagingAndSortingRepository<CustomerModel, Long>{
	CustomerModel findByMobile(String mobile);
	CustomerModel findByEmail(String email);
	CustomerModel findByReferralCode(String referralCode);
}
