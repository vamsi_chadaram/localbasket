package com.localbasket.dao;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.localbasket.model.ReviewModel;

@Repository
public interface ReviewDao extends PagingAndSortingRepository<ReviewModel, Long>{
}
