package com.localbasket.dao;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.localbasket.model.PriceRowModel;

public interface PriceRowDao extends PagingAndSortingRepository<PriceRowModel, Long>{

}
