package com.localbasket.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.localbasket.model.wcms.ContentPageModel;
import com.localbasket.model.wcms.PositionModel;

@Repository
public interface PositionDao extends JpaRepository<PositionModel, Long>{
	PositionModel findByPosition(String position);
	List<PositionModel> findByContentPage(ContentPageModel contentPage);
}
