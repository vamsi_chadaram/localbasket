package com.localbasket.dao;

import org.springframework.data.repository.CrudRepository;

import com.localbasket.model.RootCategoryModel;

public interface RootCategoryDao extends CrudRepository<RootCategoryModel, Long>{
	
}
