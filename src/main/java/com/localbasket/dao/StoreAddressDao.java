package com.localbasket.dao;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.localbasket.model.StoreAddressModel;

@Repository
public interface StoreAddressDao extends PagingAndSortingRepository<StoreAddressModel, Long> {

}
