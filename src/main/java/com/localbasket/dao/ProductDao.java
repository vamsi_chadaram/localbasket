package com.localbasket.dao;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.localbasket.model.BrandCategoryModel;
import com.localbasket.model.CategoryModel;
import com.localbasket.model.ProductModel;
import com.localbasket.model.StoreModel;

public interface ProductDao extends PagingAndSortingRepository<ProductModel, Long>{
	ProductModel findByIdAndStore(Long id, StoreModel store);
	ProductModel findByIdAndStatus(Long productId, boolean status);
	List<ProductModel> findByStore(StoreModel storeModel);
	Page<ProductModel> findByCategoryAndStore(CategoryModel category,StoreModel store, Pageable paging);
	Page<ProductModel> findByCategoryAndStoreAndBrandCategory(CategoryModel category, StoreModel store,BrandCategoryModel brandCategory,
			Pageable paging);
}
