package com.localbasket.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.localbasket.model.CustomerAddressModel;
import com.localbasket.model.CustomerModel;

public interface CustomerAddressDao extends CrudRepository<CustomerAddressModel, Long>{
	List<CustomerAddressModel> findByCustomer(CustomerModel customer);
	CustomerAddressModel findByDefaultAddress(boolean defaultAddress);
}
