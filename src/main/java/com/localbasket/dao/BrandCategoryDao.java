package com.localbasket.dao;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.localbasket.model.BrandCategoryModel;
import com.localbasket.model.CategoryModel;

@Repository
public interface BrandCategoryDao extends PagingAndSortingRepository<BrandCategoryModel, Long>{
	List<BrandCategoryModel> findByCategory(CategoryModel category);
}
