package com.localbasket.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.localbasket.model.AreaModel;
import com.localbasket.model.DeliveryPersonModel;

@Repository
public interface DeliveryPersonDao extends CrudRepository<DeliveryPersonModel, Long>{
	List<DeliveryPersonModel> findByActive(boolean active);
	DeliveryPersonModel findByEmailAndActive(String email, boolean active);
	List<DeliveryPersonModel> findByArea(AreaModel areaModel);
	DeliveryPersonModel findByEmail(String email);
}
