package com.localbasket.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.localbasket.model.DeliveryModeModel;

@Repository
public interface DeliveryModeDao extends JpaRepository<DeliveryModeModel, Long>{
	DeliveryModeModel findByMode(String mode);
}
