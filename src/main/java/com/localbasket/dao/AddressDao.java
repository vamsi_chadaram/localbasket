package com.localbasket.dao;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.localbasket.model.AddressModel;

@Repository
public interface AddressDao extends PagingAndSortingRepository<AddressModel, Long>{

}
