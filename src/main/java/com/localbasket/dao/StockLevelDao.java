package com.localbasket.dao;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.localbasket.model.StockLevelModel;

public interface StockLevelDao extends PagingAndSortingRepository<StockLevelModel, Long>{

}
