package com.localbasket.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.localbasket.model.CityModel;
import com.localbasket.model.ZoneModel;

@Repository
public interface ZoneDao extends JpaRepository<ZoneModel, Long>{
	List<ZoneModel> findByCity(CityModel city);
}
