package com.localbasket.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.localbasket.model.AbstractOrderEntryModel;
import com.localbasket.model.ProductModel;

@Repository
public interface OrderEntryDao extends CrudRepository<AbstractOrderEntryModel, Long> {
	AbstractOrderEntryModel findByProductAndCart(ProductModel product, Long cartId);
	List<AbstractOrderEntryModel> findByCart(Long cartId);
}
