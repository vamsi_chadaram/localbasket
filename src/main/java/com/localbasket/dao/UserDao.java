package com.localbasket.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.localbasket.model.UserModel;

@Repository
public interface UserDao extends CrudRepository<UserModel, Long>{
	UserModel findByEmail(String username);
	UserModel findByMobile(String mobile);
	UserModel findByReferralCode(String referralCode);
}
