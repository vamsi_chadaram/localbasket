package com.localbasket.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.localbasket.model.MediaPositionModel;
import com.localbasket.model.wcms.PositionModel;

@Repository
public interface MediaPositionDao extends JpaRepository<MediaPositionModel, Long>{
	List<MediaPositionModel> findByPosition(PositionModel position);
}
