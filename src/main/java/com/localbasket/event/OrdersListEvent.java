package com.localbasket.event;

import org.springframework.context.ApplicationEvent;

import com.localbasket.data.MailData;

public class OrdersListEvent extends ApplicationEvent{

	private static final long serialVersionUID = 1L;
	private MailData message;
	public OrdersListEvent(Object source, MailData message) {
		super(source);
		this.message= message;
	}
	public MailData getMessage() {
		return message;
	}

}
