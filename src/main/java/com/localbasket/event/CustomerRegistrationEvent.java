package com.localbasket.event;

import org.springframework.context.ApplicationEvent;

import com.localbasket.data.MailData;


public class CustomerRegistrationEvent extends ApplicationEvent{
	private static final long serialVersionUID = 1L;
	private MailData message;
	
	public CustomerRegistrationEvent(Object source, MailData message) {
		super(source);
		this.message= message;
	}
	public MailData getMessage() {
	        return message;
	}
}
