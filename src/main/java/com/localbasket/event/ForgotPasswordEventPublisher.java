package com.localbasket.event;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import com.localbasket.data.MailData;


@Component
public class ForgotPasswordEventPublisher {
	@Autowired
	private ApplicationEventPublisher applicationEventPublisher;

	@Async
	public void publishCustomEvent(final MailData mailData) {
		ForgotPasswordEvent forgotPasswordEvent = new ForgotPasswordEvent(this, mailData);
		applicationEventPublisher.publishEvent(forgotPasswordEvent);
	}
}
