package com.localbasket.event;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import com.localbasket.data.MailData;

@Component
public class OrderConfirmationEventPublisher {

	@Autowired
	private ApplicationEventPublisher applicationEventPublisher;

	@Async
	public void publishCustomEvent(final MailData mailData) {
		OrderConfirmationEvent customSpringEvent = new OrderConfirmationEvent(this, mailData);
		applicationEventPublisher.publishEvent(customSpringEvent);
	}
	
	@Async
	public void sendOrdersBetweenDates(final MailData mailData) {
		OrdersListEvent orderListEvent= new OrdersListEvent(this, mailData);
		applicationEventPublisher.publishEvent(orderListEvent);
	}
}