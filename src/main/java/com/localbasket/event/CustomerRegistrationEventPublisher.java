package com.localbasket.event;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import com.localbasket.data.MailData;

@Component
public class CustomerRegistrationEventPublisher {

	@Autowired
	private ApplicationEventPublisher applicationEventPublisher;

	@Async
	public void publishCustomEvent(final MailData mailData) {
		CustomerRegistrationEvent customSpringEvent = new CustomerRegistrationEvent(this, mailData);
		applicationEventPublisher.publishEvent(customSpringEvent);
	}
}
