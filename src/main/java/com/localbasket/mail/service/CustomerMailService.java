package com.localbasket.mail.service;

import java.io.StringWriter;
import java.util.Map;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import com.localbasket.data.MailData;
import com.localbasket.service.MailService;


@Service("customerMailService")
public class CustomerMailService implements MailService{

	@Autowired
	private JavaMailSender mailSender;

	@Autowired
	private VelocityEngine velocityEngine;

	@Override
	public void sendEmail(MailData mail) {
		
		MimeMessage mimeMessage = mailSender.createMimeMessage();

		try {
			MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage, true);

			mimeMessageHelper.setSubject("Thank you for joining in SkillRat");
			mimeMessageHelper.setFrom("sales@localbaskethd.com");
			mimeMessageHelper.setTo(mail.getMailTo());
			mail.setMailContent(geContentFromTemplate(mail.getModel()));
			mimeMessageHelper.setText(mail.getMailContent(), true);
			
			mailSender.send(mimeMessageHelper.getMimeMessage());
		} catch (MessagingException e) {
			e.printStackTrace();
		}
	}

	public String geContentFromTemplate(Map<String, Object> model) {
		VelocityContext context = new VelocityContext();
		context.put("firstName",model.get("firstName"));
		StringWriter stringWriter = new StringWriter();
		velocityEngine.mergeTemplate("template/registration.vm", "UTF-8", context, stringWriter);
		String text = stringWriter.toString();

		return text;
	}

}
