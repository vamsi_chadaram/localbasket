package com.localbasket.mail.service;

import java.io.StringWriter;
import java.util.Map;
import java.util.Set;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import com.localbasket.data.MailData;
import com.localbasket.model.AbstractOrderEntryModel;
import com.localbasket.model.OrderModel;
import com.localbasket.service.MailService;

@Service("orderConfirmationMailService")
public class OrderConfirmationMailService implements MailService{

	@Autowired
	private JavaMailSender mailSender;

	@Autowired
	private VelocityEngine velocityEngine;
	
	@Override
	public void sendEmail(MailData mail) {
		
		MimeMessage mimeMessage = mailSender.createMimeMessage();

		try {
			MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage, true);

			mimeMessageHelper.setSubject("Order confirmation");
			mimeMessageHelper.setFrom("sales@localbaskethd.com");
			mimeMessageHelper.setTo(mail.getMailTo());
			mail.setMailContent(geContentFromTemplate(mail.getModel()));
			mimeMessageHelper.setText(mail.getMailContent(), true);
			
			mailSender.send(mimeMessageHelper.getMimeMessage());
		} catch (MessagingException e) {
			e.printStackTrace();
		}
	}
	
	public String geContentFromTemplate(Map<String, Object> model) {
		
		OrderModel order= (OrderModel) model.get("order");
		
		
		Set<AbstractOrderEntryModel> entries=order.getOrderEntries();
		
		VelocityContext context = new VelocityContext();
		context.put("order",model.get("order"));
		context.put("customer",order.getCustomer());
		context.put("address",order.getDeliveryAddress());
		context.put("store", order.getStore());
		context.put("entries", entries);
		
		StringWriter stringWriter = new StringWriter();
		velocityEngine.mergeTemplate("template/orderNotification.vm", "UTF-8", context, stringWriter);
		String text = stringWriter.toString();

		return text;
	}

}
