package com.localbasket.cart.data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

public class CommerceCartData {
	
	@NotNull
	private Long productCode;
	
	@NotNull
	private Long storeId;
	
	@Min(1)
	private int qty;
	
	@NotNull
	private Long customerId;
	
	public Long getProductCode() {
		return productCode;
	}
	public void setProductCode(Long productCode) {
		this.productCode = productCode;
	}
	public Long getStoreId() {
		return storeId;
	}
	public void setStoreId(Long storeId) {
		this.storeId = storeId;
	}
	public int getQty() {
		return qty;
	}
	public void setQty(int qty) {
		this.qty = qty;
	}
	public Long getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}
	@Override
	public String toString() {
		return "CommerceCartData [productCode=" + productCode + ", storeId=" + storeId + ", qty=" + qty
				+ ", customerId=" + customerId + "]";
	}
	
}
