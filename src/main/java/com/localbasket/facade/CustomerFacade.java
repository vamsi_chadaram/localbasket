package com.localbasket.facade;

import com.localbasket.data.CustomerData;

public interface CustomerFacade {
	CustomerData createCustomer(CustomerData customer);
	CustomerData getCustomer(Long customerId);
	boolean checkCustomer(CustomerData customerData);
}
