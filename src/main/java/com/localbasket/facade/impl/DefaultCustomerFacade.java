package com.localbasket.facade.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;
import com.localbasket.data.CustomerData;
import com.localbasket.facade.CustomerFacade;
import com.localbasket.model.CustomerModel;
import com.localbasket.service.CustomerService;

@Component
public class DefaultCustomerFacade implements CustomerFacade{

	@Autowired
	private CustomerService customerService;
	
	@Override
	public CustomerData createCustomer(CustomerData customer){
		CustomerModel customerModel= null;
		if(null != customer.getId() && null != customerService.findById(customer.getId())) {
			customerModel= customerService.findById(customer.getId());
		}else {
			customerModel= new CustomerModel();
			BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
			customerModel.setPassword(encoder.encode(customer.getPassword()));
			customerModel.setUserType("USER");
		}
		
		customerModel.setFirstName(customer.getFirstName());
		customerModel.setLastName(customer.getLastName());
		customerModel.setEmail(customer.getEmail());
		customerModel.setMobile(customer.getMobile());
		
		if(null != customer.getFcmToken()) {
			customerModel.setFcmToken(customer.getFcmToken());
		}
		CustomerModel updateCustomer=customerService.save(customerModel);
		return convertCustomer(updateCustomer);
	}

	@Override
	public CustomerData getCustomer(Long customerId) {
		CustomerModel customer=customerService.findById(customerId);
		return convertCustomer(customer);
	}
	
	public CustomerData convertCustomer(CustomerModel customer) {
		CustomerData customerData= new CustomerData();
		customerData.setId(customer.getId());
		customerData.setEmail(customer.getEmail());
		customerData.setFirstName(customer.getFirstName());
		customerData.setLastName(customer.getLastName());
		customerData.setMobile(customer.getMobile());
		return customerData;
	}

	@Override
	public boolean checkCustomer(CustomerData customerData) {
		if(null != customerService.findByMobile(customerData.getMobile()) ||
				null != customerService.findByUsername(customerData.getEmail())) {
			return true;
		}
		return false;
	}
}
