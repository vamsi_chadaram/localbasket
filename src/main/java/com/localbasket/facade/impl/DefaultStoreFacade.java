package com.localbasket.facade.impl;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.localbasket.data.StoreData;
import com.localbasket.facade.StoreFacade;
import com.localbasket.model.StoreModel;
import com.localbasket.service.StoreService;

@Component
public class DefaultStoreFacade implements StoreFacade{

	@Autowired
	private StoreService storeService;
	
	@Autowired
	private ModelMapper modelMapper;
	
	@Override
	public StoreData createStore(StoreModel store) {
		StoreModel storeModel= storeService.save(store);
		
		if(null != store.getFcmToken()) {
			storeModel.setFcmToken(store.getFcmToken());
		}
		
		return modelMapper.map(storeModel, StoreData.class);
	}

}
