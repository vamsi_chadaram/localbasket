package com.localbasket.facade;

import com.localbasket.data.StoreData;
import com.localbasket.model.StoreModel;

public interface StoreFacade {
	StoreData createStore(StoreModel store);
}
