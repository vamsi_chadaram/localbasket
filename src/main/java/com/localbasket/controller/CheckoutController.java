package com.localbasket.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.localbasket.exception.InvalidAccessException;
import com.localbasket.exception.OrderNotFoundException;
import com.localbasket.model.AddressModel;
import com.localbasket.model.CartModel;
import com.localbasket.model.CustomerAddressModel;
import com.localbasket.model.CustomerModel;
import com.localbasket.model.PaymentModeModel;
import com.localbasket.service.AddressService;
import com.localbasket.service.CartService;
import com.localbasket.service.CustomerService;
import com.localbasket.service.PaymentModeService;

@RestController
@RequestMapping("/api/checkout")
public class CheckoutController {
	
	@Autowired
	private AddressService addressService;
	
	@Autowired
	private CustomerService customerService;
	
	@Autowired
	private CartService cartService;
	
	@Autowired
	private PaymentModeService paymentModeService;
	
	@GetMapping("/available-address")
	public List<CustomerAddressModel> getDeliveryAddress() {
		CustomerModel customer= (CustomerModel) customerService.getCurrentUser();
		return addressService.getCustomerAddresses(customer);
	}
	
	@PostMapping("/delivery-address")
	public CartModel addDeliveryAddress(@RequestBody AddressModel address) throws InvalidAccessException {
		
		CustomerModel customer= (CustomerModel) customerService.getCurrentUser();
		
		if(null == customer) {
			throw new InvalidAccessException("Unkown customer");
		}
		CartModel cart=cartService.getCurrentCart();
		AddressModel updatedAddress=addressService.save(address);
		CustomerAddressModel customerAddress= new CustomerAddressModel();
		customerAddress.setAddress(updatedAddress);
		customerAddress.setCustomer(customer);
		addressService.saveCustomerAddress(customerAddress);
		cart.setDeliveryAddress(updatedAddress);
		cartService.save(cart);
		return cartService.getCurrentCart();
	}
	
	@PostMapping("/select-address")
	public CartModel selectDeliveryAddress(@RequestParam(name = "addressId", required = false) Long addressId) throws OrderNotFoundException {
		AddressModel address=addressService.findById(addressId);
		CartModel cartModel=cartService.getCurrentCart();
		cartModel.setDeliveryAddress(address);
		cartService.save(cartModel);
		return cartService.getCurrentCart();
	}
	
	@GetMapping(value="/payment-modes")
	public List<PaymentModeModel> getPaymentModes(){
		return paymentModeService.findAll();
	}
	
	@PostMapping("/select-paymentmode")
	public CartModel selectPaymentMode(@RequestParam(name = "paymentModeId", required = false)Long paymentModeId){
		PaymentModeModel paymentModeModel=paymentModeService.findById(paymentModeId);
		CartModel cartModel=cartService.getCurrentCart();
		cartModel.setPaymentMode(paymentModeModel);
		return cartService.save(cartModel);
	}
}
