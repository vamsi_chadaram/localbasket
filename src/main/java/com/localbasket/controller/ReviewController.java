package com.localbasket.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.localbasket.model.CustomerModel;
import com.localbasket.model.ReviewModel;
import com.localbasket.service.CustomerService;
import com.localbasket.service.ReviewService;

@RestController
@RequestMapping(name="/api/review")
public class ReviewController {
	
	@Autowired
	private ReviewService reviewService;
	
	@Autowired
	private CustomerService customerService;
	
	@PostMapping("/store")
	public ReviewModel reviewStore(ReviewModel review) {
		if(null != customerService.getCurrentUser()) {
			review.setCustomer((CustomerModel) customerService.getCurrentUser());
		} else {
			throw new RuntimeException("No customer session available");
		}
		return reviewService.save(review);
	}
	
}
