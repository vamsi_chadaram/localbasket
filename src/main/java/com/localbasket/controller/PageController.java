package com.localbasket.controller;

import java.util.ArrayList;
import java.util.List;


import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.localbasket.data.MediaData;
import com.localbasket.data.MediaPositionData;
import com.localbasket.data.PageData;
import com.localbasket.data.PositionData;
import com.localbasket.data.RootCategoryData;
import com.localbasket.model.MediaModel;
import com.localbasket.model.MediaPositionModel;
import com.localbasket.model.RootCategoryModel;
import com.localbasket.model.wcms.ContentPageModel;
import com.localbasket.model.wcms.PositionModel;
import com.localbasket.service.CMSService;

@RestController
@RequestMapping("/page")
public class PageController {
	
	@Autowired
	private CMSService cmsService;
	
	@Autowired
	private ModelMapper modelMapper;
	
	@GetMapping("/{pageCode}")
	public PageData getPage(@PathVariable("pageCode") String pageCode) {
		ContentPageModel contentPage= cmsService.getPage(pageCode);
		List<PositionModel>	positions=cmsService.getPositionsByPage(contentPage);
		List<MediaPositionData> mediaWithPositions= new ArrayList<>();
		for(PositionModel position: positions) {
			MediaPositionData mediaPositionData=new MediaPositionData();
			PositionData positionData= modelMapper.map(position, PositionData.class);
			mediaPositionData.setPosition(positionData);
			
			List<MediaPositionModel> mediaPositions= cmsService.findByPosition(position);
			List<MediaData> medias= new ArrayList<>();
			for(MediaPositionModel mediaPosition: mediaPositions) {
				MediaModel media= mediaPosition.getMedia();
				MediaData mediaData= modelMapper.map(media, MediaData.class);
				
				if(null != media.getType() && media.getType().equals("CATEGORY")) {
					RootCategoryModel rootCategory= media.getRootCategory();
					RootCategoryData rootCategoryData= modelMapper.map(rootCategory, RootCategoryData.class);
					mediaData.setCategory(rootCategoryData);
				}
				medias.add(mediaData);
			}
			mediaPositionData.setMedias(medias);
			mediaWithPositions.add(mediaPositionData);
		}
		PageData page=modelMapper.map(contentPage, PageData.class);
		page.setMediaWithPositions(mediaWithPositions);
		return page;
	}
	
	@PostMapping("/create")
	public PageData createPage(@RequestBody ContentPageModel contentPage) {
		ContentPageModel contentPageModel= cmsService.createPage(contentPage);
		return modelMapper.map(contentPageModel, PageData.class);
	}
	
	@PostMapping("/position/create")
	public PositionData createPosition(@RequestBody PositionModel position) {
		PositionModel positionModel= cmsService.createPosition(position);
		return modelMapper.map(positionModel, PositionData.class);
	}
	
	@PostMapping("/media-position/create")
	public MediaPositionData createMediaPosition(@RequestBody MediaPositionModel mediaPosition) {
		MediaPositionModel positionModel= cmsService.createMediaPosition(mediaPosition);
		return modelMapper.map(positionModel, MediaPositionData.class);
	}
}
