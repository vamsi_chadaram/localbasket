package com.localbasket.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.localbasket.exception.InvalidAccessException;
import com.localbasket.model.BrandCategoryModel;
import com.localbasket.model.CategoryModel;
import com.localbasket.model.MediaModel;
import com.localbasket.model.PriceRowModel;
import com.localbasket.model.ProductModel;
import com.localbasket.model.StockLevelModel;
import com.localbasket.model.StoreModel;
import com.localbasket.service.BrandCategoryService;
import com.localbasket.service.CategoryService;
import com.localbasket.service.ImageStorageService;
import com.localbasket.service.PriceRowService;
import com.localbasket.service.ProductService;
import com.localbasket.service.StockService;
import com.localbasket.service.StoreService;

@RestController
@RequestMapping("/api/product")
public class ProductController {

	@Autowired
	private ProductService productService;

	@Autowired
	private PriceRowService priceRowService;

	@Autowired
	private StoreService storeService;

	@Autowired
	private StockService stockService;

	@Autowired
	private CategoryService categoryService;

	@Autowired
	private BrandCategoryService brandCategoryService;
	
	@Autowired
	private ImageStorageService imageStorageService;
	

	@PostMapping("/create")
	public ProductModel createProduct(@RequestBody ProductModel productModel) throws InvalidAccessException {
		StoreModel storeModel = storeService.getCurrentStoreUser();
		checkStore(storeModel);
		productModel.setStore(storeModel);
		return productService.save(productModel);
	}

	@GetMapping
	public ProductModel productDetails(@RequestParam("productCode") Long productId) {
		return productService.findById(productId);
	}

	@GetMapping("/list")
	public Page<ProductModel> getProducts(@RequestParam(defaultValue = "0") Integer pageNo,
			@RequestParam(defaultValue = "10") Integer pageSize, @RequestParam(defaultValue = "id") String sortBy,
			@RequestParam Long categoryId, @RequestParam Long storeId,
			@RequestParam(required = false) Long brandCategoryId) {
		StoreModel store = storeService.findById(storeId);
		CategoryModel category = categoryService.findById(categoryId);
		Pageable paging = PageRequest.of(pageNo, pageSize, Sort.by(sortBy));

		if (null != brandCategoryId) {
			BrandCategoryModel brandCategory = brandCategoryService.findById(brandCategoryId);
			return productService.findByCategoryAndStoreAndBrandCategory(category, store, brandCategory, paging);
		}
		return productService.findByCategoryAndStore(category, store, paging);
	}

	@GetMapping("/{storeId}")
	public List<ProductModel> getProductsByStore(@PathVariable("storeId") Long storeId) {
		StoreModel storeModel = storeService.findById(storeId);
		return productService.findByStore(storeModel);
	}

	@PostMapping("/media/{productId}")
	public ProductModel updateProductImage(@RequestParam(value = "image", required = true) MultipartFile image,
			@PathVariable("productId") Long productId) throws Exception {

		StoreModel store = storeService.getCurrentStoreUser();

		checkStore(store);
		
		if (null == image) {
			throw new Exception("File not available");
		}
		ProductModel product = productService.findByIdAndStore(productId,store);
		if (null == product) {
			throw new Exception("Product not found exception");
		}
		MediaModel media=imageStorageService.saveFile(image);
		product.setMedia(media);
		product.setImageUrl(media.getUrl());
		return productService.save(product);
	}

	@PostMapping(value = "/price")
	public ProductModel updateProductPrice(@RequestBody ProductModel product) throws InvalidAccessException {
		
		StoreModel store = storeService.getCurrentStoreUser();

		checkStore(store);
		ProductModel productModel = getProductByStore(product, store);
		
		PriceRowModel newPriceRow = product.getPriceRow();
		PriceRowModel priceRow = productModel.getPriceRow();
		if (null != priceRow) {
			newPriceRow.setId(priceRow.getId());
		}
		PriceRowModel updatedPriceRow = priceRowService.save(newPriceRow);
		productModel.setPriceRow(updatedPriceRow);
		return productService.save(productModel);
	}

	@PostMapping("/stock")
	public ProductModel updateStock(@RequestBody ProductModel product) throws InvalidAccessException {

		StoreModel store = storeService.getCurrentStoreUser();
		checkStore(store);
		ProductModel productModel = getProductByStore(product, store);
		StockLevelModel newStockLevel = product.getStockLevel();
		StockLevelModel stock = productModel.getStockLevel();
		if (null != stock) {
			newStockLevel.setId(stock.getId());
		}
		StockLevelModel updatedStockLevel = stockService.save(newStockLevel);
		productModel.setStockLevel(updatedStockLevel);
		return productService.save(productModel);
	}


	@PostMapping(value = "/discontinue")
	public ProductModel discontinue(@RequestBody ProductModel product) throws InvalidAccessException {
		StoreModel store = storeService.getCurrentStoreUser();
		checkStore(store);
		ProductModel productModel = getProductByStore(product, store);
		productModel.setStatus(product.isStatus());
		return productService.save(productModel);
	}

	private ProductModel getProductByStore(ProductModel product, StoreModel store) {
		return productService.findByIdAndStore(product.getId(), store);
	}
	
	private void checkStore(StoreModel store) throws InvalidAccessException {
		if (null == store) {
			throw new InvalidAccessException("Unkown store user");
		}
	}

}
