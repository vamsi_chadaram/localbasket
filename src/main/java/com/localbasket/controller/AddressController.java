package com.localbasket.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.localbasket.model.AddressModel;
import com.localbasket.model.AreaModel;
import com.localbasket.model.CityModel;
import com.localbasket.model.CustomerAddressModel;
import com.localbasket.model.CustomerModel;
import com.localbasket.model.ZoneModel;
import com.localbasket.service.AddressService;
import com.localbasket.service.CustomerService;
import com.localbasket.service.ZoneService;

@RestController
@RequestMapping("/api/address")
public class AddressController {

	@Autowired
	private AddressService addressService;

	@Autowired
	private CustomerService customerService;

	@Autowired
	private ZoneService zoneService;

	@PostMapping("/createAddress")
	public AddressModel create(@RequestBody AddressModel address) {
		AddressModel addressModel = addressService.save(address);
		return addressModel;
	}

	@GetMapping("/customer/{customerId}")
	public List<AddressModel> getCustomerAddresses(@PathVariable("customerId") Long customerId) {
		return addressService.getCustomerAddressById(customerId);
	}

	@PostMapping("/createArea")
	public AreaModel create(@RequestBody AreaModel area) {
		AreaModel areaModel = addressService.save(area);
		return areaModel;
	}

	@GetMapping("/areas")
	public List<AreaModel> getAreasByCity(Long cityId) {
		return addressService.getAreasByCity(cityId);
	}

	@PostMapping("/createCity")
	public CityModel create(@RequestBody CityModel city) {
		CityModel cityModel = addressService.save(city);
		return cityModel;

	}

	@GetMapping("/cities")
	public List<CityModel> getCities() {
		return addressService.getCities();
	}

	@GetMapping("/remove")
	public boolean removeAddress(Long addressId) {

		CustomerModel customer = (CustomerModel) customerService.getCurrentUser();

		if (null == customer) {
			throw new RuntimeException("Please login");
		}

		CustomerAddressModel customerAddress = addressService.getCustomerAddress(addressId);
		addressService.remove(customerAddress.getAddress());
		addressService.removeCustomerAddress(customerAddress);
		return true;
	}

	@GetMapping("/zones-by-city")
	public List<ZoneModel> getZonesByCity(Long cityId) {
		CityModel city = addressService.findCityById(cityId);
		return zoneService.getZonesByCity(city);
	}
}
