package com.localbasket.controller;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.localbasket.model.MediaModel;
import com.localbasket.service.ImageStorageService;
import com.localbasket.service.MediaService;

@RestController
@RequestMapping("/image")
public class ImageStorageController {
	
	@Autowired
	private ImageStorageService imageStorageService;
	
	@Autowired
	private MediaService mediaService;
	
	@PutMapping("/save")
	public MediaModel saveImage(@RequestParam(value = "image", required = true) MultipartFile image) throws IOException {
		return imageStorageService.saveFile(image);
	}
	
	@PostMapping("/media/update")
	public MediaModel updateMedia(@RequestBody MediaModel media) {
		return mediaService.createMedia(media);
	}
}
