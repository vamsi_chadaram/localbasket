package com.localbasket.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.localbasket.model.OneSignalModel;
import com.localbasket.service.OneSignalService;

@RestController
@RequestMapping("/onesignal")
public class OneSignalController {
	
	@Autowired
	private OneSignalService oneSignalService;
	
	@PostMapping("/create")
	public OneSignalModel createOneSignal(@RequestBody OneSignalModel oneSignalModel) {
		return oneSignalService.createOneSignal(oneSignalModel);
	}
	
	@GetMapping("/find")
	public OneSignalModel getOneSignal(Long id) {
		return oneSignalService.getOneSignal(id);
	}
	
	@GetMapping("/find-by-customer")
	public OneSignalModel findByCustomer(Long customertId) {
		return null;
	}
	
	@GetMapping("/all")
	public List<OneSignalModel> findAll() {
		return oneSignalService.findAll();
	}
	
	@DeleteMapping("/delete")
	public List<OneSignalModel> delete(Long id) {
		oneSignalService.delete(id);
		return oneSignalService.findAll();
	}
}
