package com.localbasket.controller;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.localbasket.enumeration.OrderStatus;
import com.localbasket.exception.OrderNotFoundException;
import com.localbasket.exception.StockUnavailableException;
import com.localbasket.model.CartModel;
import com.localbasket.model.DeliveryPersonModel;
import com.localbasket.model.OrderModel;
import com.localbasket.model.PaymentModeModel;
import com.localbasket.model.StoreModel;
import com.localbasket.service.CartService;
import com.localbasket.service.CustomerService;
import com.localbasket.service.DeliveryPersonService;
import com.localbasket.service.OrderService;
import com.localbasket.service.PaymentModeService;
import com.localbasket.service.StoreService;

@RestController
@RequestMapping("/api/order")
public class OrderController {

	@Autowired
	private OrderService orderService;
	
	@Autowired
	private CartService cartService;
	
	@Autowired
	private StoreService storeService;
	
	@Autowired
	private CustomerService customerService;
	
	@Autowired
	private DeliveryPersonService deliveryPersonService;
	
	@Autowired
	private PaymentModeService paymentModeService;
	
	@PostMapping("/place-order")
	public OrderModel placeOrder() throws CloneNotSupportedException, StockUnavailableException {
		
		if(null == customerService.getCurrentUser()) {
			throw new RuntimeException("Please login");
		}
		
		CartModel cartModel=cartService.getCurrentCart();
		
		if(null == cartModel.getDeliveryAddress()) {
			throw new RuntimeException("No delivery address present");
		}
		
		if(null == cartModel.getPaymentMode()) {
			PaymentModeModel paymentMode=paymentModeService.getPaymentMode("COD");
			cartModel.setPaymentMode(paymentMode);
		}
		
		return orderService.placeOrder(cartModel);
	}
	
	@PostMapping("/accept/{orderId}")
	public OrderModel acceptOrder(@PathVariable("orderId") Long orderId ) throws OrderNotFoundException {
		
		if(null == storeService.getCurrentStoreUser()) {
			throw new RuntimeException("Please login");
		}
		
		OrderModel order=orderService.findById(orderId);
		order.setOrderStatus(OrderStatus.CONFIRMED);
		return orderService.save(order);
	}
	
	@PostMapping("/reject/{orderId}")
	public OrderModel rejectOrder(@PathVariable("orderId") Long orderId ) throws OrderNotFoundException {
		
		if(null == storeService.getCurrentStoreUser()) {
			throw new RuntimeException("Please login");
		}
		
		OrderModel order=orderService.findById(orderId);
		order.setOrderStatus(OrderStatus.REJECTED);
		return orderService.save(order);
	}
	
	@PostMapping("/outfordelivery/{orderId}") 
	public OrderModel outForDelivery(@PathVariable("orderId") Long orderId ) throws OrderNotFoundException {
		OrderModel order=orderService.findById(orderId);
		order.setOrderStatus(OrderStatus.OUT_FOR_DELIVERY);
		return orderService.save(order);
	}
	
	@PostMapping("/delivered/{orderId}")
	public OrderModel confirmDelivery(@PathVariable("orderId") Long orderId ) throws OrderNotFoundException {
		
		if(null == storeService.getCurrentStoreUser()) {
			throw new RuntimeException("Please login");
		}
		
		OrderModel order=orderService.findById(orderId);
		order.setOrderStatus(OrderStatus.DELIVERED);
		DeliveryPersonModel deliveryPerson= order.getDeliveryPerson();
		deliveryPerson.setAvailable(true);
		deliveryPersonService.save(deliveryPerson);
		return orderService.save(order);
	}
	
	@GetMapping(value="/store-orders/{storeId}/{status}")
	public List<OrderModel> findOrdersByStoreAndStatus(@PathVariable("storeId")Long storeId,
			@PathVariable("status")int orderStatus) throws OrderNotFoundException{
		StoreModel store= storeService.findById(storeId);
		
		switch(orderStatus) {
		case 0:
			return orderService.findOrdersByStoreAndOrderStatus(store,OrderStatus.PLACED);
		case 1:
			return orderService.findOrdersByStoreAndOrderStatus(store,OrderStatus.IN_PROGRESS);
		case 2:
			return orderService.findOrdersByStoreAndOrderStatus(store,OrderStatus.CONFIRMED);
		case 3:
			return orderService.findOrdersByStoreAndOrderStatus(store,OrderStatus.REJECTED);
		case 4:
			return orderService.findOrdersByStoreAndOrderStatus(store,OrderStatus.OUT_FOR_DELIVERY);
		case 5:
			return orderService.findOrdersByStoreAndOrderStatus(store,OrderStatus.REJECTED);
		default:
			return orderService.findOrdersByStoreAndOrderStatus(store,OrderStatus.DELIVERED);
		}
	}
	
	@GetMapping(value="/store-orders/{storeId}")
	public List<OrderModel> findOrdersByStore(@PathVariable("storeId")Long storeId) throws OrderNotFoundException{
		
		if(null == storeService.getCurrentStoreUser()) {
			throw new RuntimeException("Please login");
		}
		
		StoreModel store= storeService.findById(storeId);
		return orderService.findOrdersByStore(store);
	}
	
	@PostMapping(value="/assign-deliverypersion/{personId}/{orderId}")
	public OrderModel assignDeliveryPerson(@PathVariable("personId") Long personId ,@PathVariable("orderId") Long orderId) throws OrderNotFoundException{
		
		if(null == customerService.getCurrentUser()) {
			throw new RuntimeException("Please login");
		}
		OrderModel order= orderService.findById(orderId);
		DeliveryPersonModel deliveryPerson= deliveryPersonService.findById(personId);
		order.setDeliveryPerson(deliveryPerson);
		deliveryPerson.setAvailable(false);
		deliveryPersonService.save(deliveryPerson);
		return orderService.save(order);
	}
	
	@GetMapping("/last-twodays-orders")
	public List<OrderModel> getLastTwoDaysOrders() {
		Date date= new Date();
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.add(Calendar.DATE, -1);
		return orderService.getAllBetweenDates(c.getTime(), date);
	}
	
	@GetMapping("/orders-by-deliveryperson")
	public List<OrderModel> getDeliveryPersonOrders(){
		if(null == customerService.getCurrentUser()) {
			throw new RuntimeException("Please login");
		}
		return orderService.getPendingOrders((DeliveryPersonModel)customerService.getCurrentUser());
	}
}
