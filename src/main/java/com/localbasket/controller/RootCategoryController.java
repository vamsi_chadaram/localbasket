package com.localbasket.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.localbasket.model.RootCategoryModel;
import com.localbasket.service.RootCategoryService;

@RestController
@RequestMapping("/api/root-categories")
public class RootCategoryController{
	
	@Autowired
	private RootCategoryService rootCategoryService;
	
	@GetMapping("/list")
	public Iterable<RootCategoryModel> getRootCategories(){
		return rootCategoryService.findAll();
	}
	
	@PostMapping("/create")
	public RootCategoryModel createRootCategory(RootCategoryModel rootCategory) {
		return rootCategoryService.save(rootCategory);
	}
}
