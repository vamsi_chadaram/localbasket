package com.localbasket.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.websocket.server.PathParam;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.validator.routines.EmailValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.localbasket.data.CustomerData;
import com.localbasket.data.MailData;
import com.localbasket.event.CustomerRegistrationEventPublisher;
import com.localbasket.event.ForgotPasswordEventPublisher;
import com.localbasket.exception.DuplipcateUserException;
import com.localbasket.exception.OrderNotFoundException;
import com.localbasket.facade.CustomerFacade;
import com.localbasket.model.CustomerModel;
import com.localbasket.model.OrderModel;
import com.localbasket.model.UserModel;
import com.localbasket.service.CustomerService;
import com.localbasket.service.OrderService;
import com.localbasket.service.StoreService;

@RestController
@RequestMapping("/api/customer")
public class CustomerController {
	
	@Autowired
	private CustomerFacade customerFacade;
	
	@Autowired
	private CustomerService customerService;
	
	@Autowired
	private OrderService orderService;
	
	@Autowired
	private StoreService storeService;
	
	@Autowired
	private ForgotPasswordEventPublisher forgotPasswordEventPublisher;
	
	@Autowired //Use setter based injection
	private CustomerRegistrationEventPublisher applicationEventPublisher;
	
	@PostMapping("/create")
	public CustomerData createCustomer(@RequestBody CustomerData customer) throws DuplipcateUserException{
		
		if(!EmailValidator.getInstance().isValid(customer.getEmail())) {
			throw new RuntimeException("EMAIL IS NOT VALID");
		}
		if(null == customer.getId() && customerFacade.checkCustomer(customer)) {
			throw new DuplipcateUserException("User found with same email or mobile number");
		} else if(null == customer.getId() && null != storeService.findByContactNumber(customer.getMobile()) && 
				null != storeService.findByEmail(customer.getEmail())){
			throw new DuplipcateUserException("Store found with same email or mobile number");
		}else {
			CustomerData customerData= customerFacade.createCustomer(customer);
			MailData mail= new MailData();
			mail.setMailTo(customerData.getEmail());
			Map<String, Object> propertiesMap= new HashMap<>();
			propertiesMap.put("firstName", customerData.getFirstName());
			mail.setModel(propertiesMap);
			
			try {
				applicationEventPublisher.publishCustomEvent(mail);
			} catch(Exception ex) {
			}
			
			return customerData;
		}
	}
	
	@GetMapping("/{customerId}")
	public CustomerData getCustomer(@PathParam("customerId")Long customerId) {
		return customerFacade.getCustomer(customerId);
	}
	
	@GetMapping("/current")
	public UserModel getCurrentCustomer() throws OrderNotFoundException {
		return customerService.getCurrentUser();
	}
	
	@PostMapping("/update-password")
	public CustomerData updatePassword(CustomerData customerData){
		CustomerData customer=customerFacade.getCustomer(customerData.getId());
		customer.setPassword(customerData.getPassword());
		return customerFacade.createCustomer(customer);
	}
	
	@GetMapping("/orders")
	public List<OrderModel> getOrders(){
		CustomerModel customer=(CustomerModel) customerService.getCurrentUser();
		return orderService.findByCustomer(customer);
	}
	
	@GetMapping("/forgot-password")
	public void forgotPassword(@RequestParam String username) {
		CustomerModel customer=customerService.findByUsername(username);
		if(null == customer) {
			customer = customerService.findByMobile(username);
		}
		if(null != customer) {
			String password=RandomStringUtils.randomAlphanumeric(8);
			customer.setPassword(new BCryptPasswordEncoder().encode(password));
			customerService.save(customer);
			MailData mail= new MailData();
			mail.setMailTo(customer.getEmail());
			Map<String, Object> propertiesMap= new HashMap<>();
			propertiesMap.put("firstName", customer.getFirstName());
			propertiesMap.put("password", password);
			mail.setModel(propertiesMap);
			
			try {
				forgotPasswordEventPublisher.publishCustomEvent(mail);
			}catch(Exception ex) {
			}
		}
	}
}
