package com.localbasket.controller;
import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.localbasket.cart.data.CommerceCartData;
import com.localbasket.data.CartData;
import com.localbasket.data.OrderEntryData;
import com.localbasket.exception.InvalidAccessException;
import com.localbasket.exception.ProductNotAvailableException;
import com.localbasket.exception.StockUnavailableException;
import com.localbasket.model.AbstractOrderEntryModel;
import com.localbasket.model.CartModel;
import com.localbasket.model.ProductModel;
import com.localbasket.model.StockLevelModel;
import com.localbasket.service.CartService;
import com.localbasket.service.CustomerService;
import com.localbasket.service.OrderEntryService;
import com.localbasket.service.ProductService;

@RestController
@RequestMapping("/api/cart")
public class CartController {
	
	private static final Logger LOG= LoggerFactory.getLogger(CartController.class);
	@Autowired
	private CartService cartService;
	
	@Autowired
	private ProductService productService;
	
	@Autowired
	private CustomerService customerService;
	
	@Autowired
	private ModelMapper modelMapper;
	
	@Autowired
	private OrderEntryService orderEntryService;
	
	@PostMapping("/add")
	public CartData addToCart(@RequestBody CommerceCartData commerceCartData) throws StockUnavailableException, ProductNotAvailableException, InvalidAccessException{
		
		if(null == customerService.getCurrentUser()) {
			LOG.info("Customer session was expired");
			throw new InvalidAccessException("Unkown customer");
		}
		ProductModel product=productService.findById(commerceCartData.getProductCode());
		if(null == product) {
			LOG.info("Product not available at the moment");
			throw new ProductNotAvailableException("Product not available at the moment");
		}
		StockLevelModel stockLevel=product.getStockLevel();
		
		if(!stockLevel.isForceAvailable() && stockLevel.getAvailableCount() < commerceCartData.getQty()) {
			throw new StockUnavailableException("Stock not found for product "+commerceCartData.getProductCode());
		}
		CartModel cartModel =cartService.addToCart(commerceCartData);
		return convertCart(cartModel);
	}

	private CartData convertCart(CartModel cartModel) {
		CartData cartData=modelMapper.map(cartModel, CartData.class);
		
		List<OrderEntryData> orderEntries= new ArrayList<>();
		if(!CollectionUtils.isEmpty(orderEntryService.findByCart(cartModel.getId()))) {
			for(AbstractOrderEntryModel abstractOrderEntryModel: orderEntryService.findByCart(cartModel.getId())) {
				orderEntries.add(modelMapper.map(abstractOrderEntryModel, OrderEntryData.class));
			}
			cartData.setOrderEntries(orderEntries);
		}
		
		return cartData;
	}
	
	@GetMapping
	public CartData getCart() {
		return convertCart(cartService.getCurrentCart());
	}
}
