package com.localbasket.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.localbasket.model.DeliveryModeModel;
import com.localbasket.service.DeliveryModeService;

@RestController
@RequestMapping("/delivery-mode")
public class DeliveryModeController {
	
	@Autowired
	private DeliveryModeService deliveryModeService;
	
	@PostMapping("/create")
	public DeliveryModeModel createDeliveryMode(@RequestBody DeliveryModeModel deliveryMode) {
		return deliveryModeService.createDeliveryMode(deliveryMode);
		
	}
}
