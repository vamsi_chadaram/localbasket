package com.localbasket.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.localbasket.exception.OrderNotFoundException;
import com.localbasket.model.CategoryModel;
import com.localbasket.service.CategoryService;

@RestController
@RequestMapping("/api/category")
public class CategoryController {

	@Autowired
	private CategoryService categoryService;
	
	@GetMapping("/list")
	public List<CategoryModel> getCategories(Long rootCategoryId) throws OrderNotFoundException{
		return categoryService.listByRootCategory(rootCategoryId);
	}
	
	@PostMapping("/create")
	public CategoryModel createCategory(@RequestBody CategoryModel category) {
		return categoryService.save(category);
	}
}
