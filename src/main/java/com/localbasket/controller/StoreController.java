package com.localbasket.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.localbasket.data.MailData;
import com.localbasket.data.StoreData;
import com.localbasket.event.ForgotPasswordEventPublisher;
import com.localbasket.exception.DuplipcateUserException;
import com.localbasket.facade.StoreFacade;
import com.localbasket.model.AddressModel;
import com.localbasket.model.MediaContainer;
import com.localbasket.model.MediaModel;
import com.localbasket.model.StoreAddressModel;
import com.localbasket.model.StoreModel;
import com.localbasket.service.AddressService;
import com.localbasket.service.CustomerService;
import com.localbasket.service.ImageStorageService;
import com.localbasket.service.MediaService;
import com.localbasket.service.StoreService;

@RestController
@RequestMapping("/api/store")
public class StoreController {
	
	@Autowired
	private ForgotPasswordEventPublisher forgotPasswordEventPublisher;
	
	@Autowired
	private StoreService storeService;
	
	@Autowired
	private AddressService addressService;
	
	@Autowired
	private StoreFacade storeFacade;
	
	@Autowired
	private ImageStorageService imageStorageService;
	
	@Autowired
	private MediaService mediaService;
	
	@Autowired
	private CustomerService customerService;
	
	@PostMapping("/create")
	public StoreData createStore(@RequestBody StoreModel storeModel) throws DuplipcateUserException {
		if(null == storeModel.getId() && null != storeService.findByEmail(storeModel.getEmail()) && 
				null !=storeService.findByContactNumber(storeModel.getContactNumber())) {
			throw new DuplipcateUserException("Duplicate store found with same email or mobile");
		} else if(null == storeModel.getId() && null != customerService.findByMobile(storeModel.getContactNumber())
				&& null != customerService.findByUsername(storeModel.getEmail())) {
			throw new DuplipcateUserException("Duplicate user found with same email or mobile");
		}
		return storeFacade.createStore(storeModel);
	}
	
	@GetMapping
	public StoreModel getCurrentStore() {
		return storeService.getCurrentStoreUser();
	}
	
	@GetMapping("/category")
	public List<StoreModel> getStoresByCategory(@RequestParam("categoryId")Long rootCategoryId){
		return storeService.getStoresByCategory(rootCategoryId);
	}
	
	@PostMapping("/available_time")
	public StoreModel updateStoreTimings(@RequestBody StoreModel store){
		StoreModel storeModel= storeService.findById(store.getId());
		storeModel.setAvailableFrom(store.getAvailableFrom());
		storeModel.setAvailableTo(store.getAvailableTo());
		return storeService.save(storeModel);
	}
	
	@PostMapping("/address")
	public StoreAddressModel addStoreAddress(@RequestBody StoreAddressModel storeAddress) {
		AddressModel address= addressService.save(storeAddress.getAddress());
		storeAddress.setAddress(address);
		return storeService.saveStoreAddress(storeAddress);
	}
	
	@PostMapping("/thumbnail")
	public StoreModel updateThumbnail(@RequestParam(value = "image", required = true) MultipartFile image) throws IOException {
		StoreModel store = storeService.getCurrentStoreUser();
		MediaModel media=imageStorageService.saveFile(image);
		
		if(null == store.getMediaContainer()) {
			MediaContainer mediaContainer= new MediaContainer();
			mediaContainer.setThumbNail(media);
			mediaService.createMediaContainer(mediaContainer);
			store.setMediaContainer(mediaContainer);
			storeService.save(store);
		} else {
			MediaContainer mediaContainer= store.getMediaContainer();
			mediaContainer.setThumbNail(media);
			mediaService.createMediaContainer(mediaContainer);
			store.setMediaContainer(mediaContainer);
		}
		return store;
	}
	
	@PostMapping("/banner")
	public StoreModel updateBanner(@RequestParam(value = "image", required = true) MultipartFile image) throws IOException {
		StoreModel store = storeService.getCurrentStoreUser();
		MediaModel media=imageStorageService.saveFile(image);
		
		if(null == store.getMediaContainer()) {
			MediaContainer mediaContainer= new MediaContainer();
			mediaContainer.setBannerMedia(media);
			mediaService.createMediaContainer(mediaContainer);
			store.setMediaContainer(mediaContainer);
			storeService.save(store);
		} else {
			MediaContainer mediaContainer= store.getMediaContainer();
			mediaContainer.setBannerMedia(media);
			mediaService.createMediaContainer(mediaContainer);
			store.setMediaContainer(mediaContainer);
		}
		return store;
	}
	
	@GetMapping("/forgot-password")
	public boolean forgotPassword(String email) {
		StoreModel store=storeService.findByEmail(email);
		
		if(null != store) {
			String password=RandomStringUtils.randomAlphanumeric(8);
			store.setPassword(new BCryptPasswordEncoder().encode(password));
			storeService.save(store);
			MailData mail= new MailData();
			mail.setMailTo(store.getEmail());
			Map<String, Object> propertiesMap= new HashMap<>();
			propertiesMap.put("firstName", store.getFirstName());
			propertiesMap.put("password", password);
			mail.setModel(propertiesMap);
			
			try {
				forgotPasswordEventPublisher.publishCustomEvent(mail);
			}catch(Exception ex) {
			}
			return true;
		}
		
		return false;
	}
} 
