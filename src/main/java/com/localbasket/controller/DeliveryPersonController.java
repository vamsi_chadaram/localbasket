package com.localbasket.controller;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.localbasket.data.DeliveryPersonData;
import com.localbasket.exception.OrderNotFoundException;
import com.localbasket.model.DeliveryPersonModel;
import com.localbasket.model.OrderModel;
import com.localbasket.service.CustomerService;
import com.localbasket.service.DeliveryPersonService;
import com.localbasket.service.OrderService;

@RestController
@RequestMapping("/api/delivery-person")
public class DeliveryPersonController {
	
	@Autowired
	private CustomerService customerService;
	
	@Autowired
	private DeliveryPersonService deliveryPersonService;
	
	@Autowired
	private OrderService orderService;
	
	@PostMapping("/save")
	public DeliveryPersonModel createDeliveryPerson(@RequestBody DeliveryPersonModel deliveryPerson) {
		
		if(null == customerService.getCurrentUser()) {
			throw new RuntimeException("Please login");
		}
		
		if(null == deliveryPerson.getId()) {
			BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
			deliveryPerson.setPassword(encoder.encode(deliveryPerson.getPassword()));
		} else {
			deliveryPerson.setPassword(deliveryPersonService.findByEmail(deliveryPerson.getEmail()).getPassword());
		}
		return deliveryPersonService.save(deliveryPerson);
	}
	
	
	
	@GetMapping("/list")
	public List<DeliveryPersonData> findAll(@RequestParam(defaultValue = "0") Integer pageNo,
			@RequestParam(defaultValue = "10") Integer pageSize, @RequestParam(defaultValue = "id") String sortBy){
		
		
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		
		List<DeliveryPersonData> deliveryPersons= new ArrayList<>();
		for(DeliveryPersonModel deliveryPerson: deliveryPersonService.findAll()) {
			DeliveryPersonData deliveryPersonData= new DeliveryPersonData();
			deliveryPersonData.setActive(deliveryPerson.isActive());
			deliveryPersonData.setAvailable(deliveryPerson.isAvailable());
			deliveryPersonData.setId(deliveryPerson.getId());
			deliveryPersonData.setFirstName(deliveryPerson.getFirstName());
			deliveryPersonData.setLastName(deliveryPerson.getLastName());
			deliveryPersonData.setMobile(deliveryPerson.getMobile());
			deliveryPersonData.setEmail(deliveryPerson.getEmail());
			deliveryPersonData.setArea(deliveryPerson.getArea());
			List<OrderModel> orders=orderService.getOrdersByDeliveryBoyWithDates(deliveryPerson, cal.getTime(), cal.getTime());
			Double totalAmount= Double.valueOf(0);
			for(OrderModel order: orders) {
				totalAmount= totalAmount+order.getTotal();
			}
			deliveryPersonData.setTotalAmount(totalAmount);
			deliveryPersons.add(deliveryPersonData);
		}
		return deliveryPersons;
	}
	
	@GetMapping("/{deliveryPersonId}")
	public List<OrderModel> findOrders(@PathVariable("deliveryPersonId") Long deliveryPersonId) throws OrderNotFoundException{
		DeliveryPersonModel deliveryPerson=deliveryPersonService.findById(deliveryPersonId);
		return deliveryPersonService.findByDeliveryPerson(deliveryPerson);
	}
	
	@GetMapping("/pending/{deliveryPersonId}")
	public List<OrderModel> getPendingOrders(@PathVariable("deliveryPersonId") Long deliveryPersonId){
		
		if(null == customerService.getCurrentUser()) {
			throw new RuntimeException("Please login");
		}
		DeliveryPersonModel deliveryPerson=deliveryPersonService.findById(deliveryPersonId);
		return orderService.getOrdersByDeliveryBoyWithDates(deliveryPerson, new Date(), new Date());
	}
}
