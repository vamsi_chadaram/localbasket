package com.localbasket.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.error.OAuth2AccessDeniedHandler;

@Configuration
@EnableResourceServer
public class ResourceServerConfig extends ResourceServerConfigurerAdapter {

	private static final String RESOURCE_ID = "resource_id";
	
	@Override
	public void configure(ResourceServerSecurityConfigurer resources) {
		resources.resourceId(RESOURCE_ID).stateless(false);
	}

	@Override
	public void configure(HttpSecurity http) throws Exception {
        
        http.
        anonymous().disable()
        .authorizeRequests()
        .antMatchers("/product/create").access("hasAnyRole('STORE','ADMIN')")
        .and().exceptionHandling().accessDeniedHandler(new OAuth2AccessDeniedHandler());
        
        http.
        anonymous().disable()
        .authorizeRequests()
        .antMatchers("/cart").access("hasAnyRole('USER', 'ADMIN')")
        .and().exceptionHandling().accessDeniedHandler(new OAuth2AccessDeniedHandler());
        
        http.
        anonymous().disable()
        .authorizeRequests()
        .antMatchers("/order/accept").access("hasAnyRole('STORE','ADMIN')")
        .and().exceptionHandling().accessDeniedHandler(new OAuth2AccessDeniedHandler());
        
        http.
        anonymous().disable()
        .authorizeRequests()
        .antMatchers("/order/reject").access("hasAnyRole('STORE','ADMIN')")
        .and().exceptionHandling().accessDeniedHandler(new OAuth2AccessDeniedHandler());
        
        http.
        anonymous().disable()
        .authorizeRequests()
        .antMatchers("/order/delivered").access("hasRole('DELIVERY_PERSON','ADMIN')")
        .and().exceptionHandling().accessDeniedHandler(new OAuth2AccessDeniedHandler());
        
        http.
        anonymous().disable()
        .authorizeRequests()
        .antMatchers("/order/store-orders").access("hasAnyRole('STORE', 'ADMIN')")
        .and().exceptionHandling().accessDeniedHandler(new OAuth2AccessDeniedHandler());
        
        http.
        anonymous().disable()
        .authorizeRequests()
        .antMatchers("/order/assign-deliverypersion").access("hasRole('ADMIN')")
        .and().exceptionHandling().accessDeniedHandler(new OAuth2AccessDeniedHandler());
        
        http.
        anonymous().disable()
        .authorizeRequests()
        .antMatchers("/delivery-person/save").access("hasRole('ADMIN')")
        .and().exceptionHandling().accessDeniedHandler(new OAuth2AccessDeniedHandler());
	}
}
