package com.localbasket.data;

import java.util.List;

public class MediaPositionData {
	private PositionData position;
	private List<MediaData> medias;
	public PositionData getPosition() {
		return position;
	}
	public void setPosition(PositionData position) {
		this.position = position;
	}
	public List<MediaData> getMedias() {
		return medias;
	}
	public void setMedias(List<MediaData> medias) {
		this.medias = medias;
	}
}
