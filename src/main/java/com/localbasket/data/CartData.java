package com.localbasket.data;

import java.util.Date;
import java.util.List;

import com.localbasket.enumeration.OrderStatus;
import com.localbasket.model.AddressModel;
import com.localbasket.model.CustomerModel;
import com.localbasket.model.PaymentModeModel;
import com.localbasket.model.StoreModel;

public class CartData {
	
	private Long id;
	
	private Integer totalQuantity;
	
	private Double subTotal;
	
	private Double deliveryCharges;
	
	private Double total;
	
	private Double discount;

	private AddressModel deliveryAddress;
	
	private AddressModel paymentAddress;
	
	private CustomerModel customer;
	
	private StoreModel store;

	private OrderStatus orderStatus;
	
	private List<OrderEntryData> orderEntries;
	
	private PaymentModeModel paymentMode;
	
	private Date createdDate;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getTotalQuantity() {
		return totalQuantity;
	}

	public void setTotalQuantity(Integer totalQuantity) {
		this.totalQuantity = totalQuantity;
	}

	public Double getSubTotal() {
		return subTotal;
	}

	public void setSubTotal(Double subTotal) {
		this.subTotal = subTotal;
	}

	public Double getDeliveryCharges() {
		return deliveryCharges;
	}

	public void setDeliveryCharges(Double deliveryCharges) {
		this.deliveryCharges = deliveryCharges;
	}

	public Double getTotal() {
		return total;
	}

	public void setTotal(Double total) {
		this.total = total;
	}

	public Double getDiscount() {
		return discount;
	}

	public void setDiscount(Double discount) {
		this.discount = discount;
	}

	public AddressModel getDeliveryAddress() {
		return deliveryAddress;
	}

	public void setDeliveryAddress(AddressModel deliveryAddress) {
		this.deliveryAddress = deliveryAddress;
	}

	public AddressModel getPaymentAddress() {
		return paymentAddress;
	}

	public void setPaymentAddress(AddressModel paymentAddress) {
		this.paymentAddress = paymentAddress;
	}

	public CustomerModel getCustomer() {
		return customer;
	}

	public void setCustomer(CustomerModel customer) {
		this.customer = customer;
	}

	public StoreModel getStore() {
		return store;
	}

	public void setStore(StoreModel store) {
		this.store = store;
	}

	public OrderStatus getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(OrderStatus orderStatus) {
		this.orderStatus = orderStatus;
	}

	public List<OrderEntryData> getOrderEntries() {
		return orderEntries;
	}

	public void setOrderEntries(List<OrderEntryData> orderEntries) {
		this.orderEntries = orderEntries;
	}

	public PaymentModeModel getPaymentMode() {
		return paymentMode;
	}

	public void setPaymentMode(PaymentModeModel paymentMode) {
		this.paymentMode = paymentMode;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
}
