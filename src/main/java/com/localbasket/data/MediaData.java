package com.localbasket.data;

import java.util.Date;


public class MediaData {
	private Long id;
	
	private String name;
	
	private String ext;
	
	private Date createdTime;
	
	private String url;

	private String type;
	
	private RootCategoryData category;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getExt() {
		return ext;
	}

	public void setExt(String ext) {
		this.ext = ext;
	}

	public Date getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(Date createdTime) {
		this.createdTime = createdTime;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public RootCategoryData getCategory() {
		return category;
	}

	public void setCategory(RootCategoryData category) {
		this.category = category;
	}
}
