package com.localbasket.data;

import java.util.List;
import java.util.Map;

public class MailData {
	private String mailFrom;
	private String mailTo;
	private String mailCC;
	private String mailBCC;
	private String mailSubject;
	private String mailContent;
	private String contentType;
	private List < Object > attachments;
	private Map < String, Object > model;
	
	public String getMailFrom() {
		return mailFrom;
	}
	public void setMailFrom(String mailFrom) {
		this.mailFrom = mailFrom;
	}
	public String getMailTo() {
		return mailTo;
	}
	public void setMailTo(String mailTo) {
		this.mailTo = mailTo;
	}
	public String getMailCC() {
		return mailCC;
	}
	public void setMailCC(String mailCC) {
		this.mailCC = mailCC;
	}
	public String getMailBCC() {
		return mailBCC;
	}
	public void setMailBCC(String mailBCC) {
		this.mailBCC = mailBCC;
	}
	public String getMailSubject() {
		return mailSubject;
	}
	public void setMailSubject(String mailSubject) {
		this.mailSubject = mailSubject;
	}
	public String getMailContent() {
		return mailContent;
	}
	public void setMailContent(String mailContent) {
		this.mailContent = mailContent;
	}
	public String getContentType() {
		return contentType;
	}
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}
	public List<Object> getAttachments() {
		return attachments;
	}
	public void setAttachments(List<Object> attachments) {
		this.attachments = attachments;
	}
	public Map<String, Object> getModel() {
		return model;
	}
	public void setModel(Map<String, Object> model) {
		this.model = model;
	}
}
