package com.localbasket.data;

import com.localbasket.model.RootCategoryModel;

public class StoreData extends UserData{
	private String storeName;
	
	private String storeCode;
	
	private boolean status;
	
	private String contactNumber;
	
	private RootCategoryModel rootCategory;
	
	private int availableFrom;
	
	private int availableTo;
	
	private Double avgRating;
	
	private Long totalReviews;

	public String getStoreName() {
		return storeName;
	}

	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}

	public String getStoreCode() {
		return storeCode;
	}

	public void setStoreCode(String storeCode) {
		this.storeCode = storeCode;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public String getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	public RootCategoryModel getRootCategory() {
		return rootCategory;
	}

	public void setRootCategory(RootCategoryModel rootCategory) {
		this.rootCategory = rootCategory;
	}

	public int getAvailableFrom() {
		return availableFrom;
	}

	public void setAvailableFrom(int availableFrom) {
		this.availableFrom = availableFrom;
	}

	public int getAvailableTo() {
		return availableTo;
	}

	public void setAvailableTo(int availableTo) {
		this.availableTo = availableTo;
	}

	public Double getAvgRating() {
		return avgRating;
	}

	public void setAvgRating(Double avgRating) {
		this.avgRating = avgRating;
	}

	public Long getTotalReviews() {
		return totalReviews;
	}

	public void setTotalReviews(Long totalReviews) {
		this.totalReviews = totalReviews;
	}
	
}
