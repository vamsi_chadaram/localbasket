package com.localbasket.data;

public class ProductData {

	private Long id;
	
	private String code;
	
	private String name;

	private String nativeName;
	
	private String summary;
	
	private String nativeSummary;
	
	private String description;
	
	private String nativeDescription;
	
	
	private PriceRowData priceRow;
	
	private boolean status;

	private boolean popular;
	
	private boolean popularCount;

	private String imageUrl;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNativeName() {
		return nativeName;
	}

	public void setNativeName(String nativeName) {
		this.nativeName = nativeName;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public String getNativeSummary() {
		return nativeSummary;
	}

	public void setNativeSummary(String nativeSummary) {
		this.nativeSummary = nativeSummary;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getNativeDescription() {
		return nativeDescription;
	}

	public void setNativeDescription(String nativeDescription) {
		this.nativeDescription = nativeDescription;
	}

	public PriceRowData getPriceRow() {
		return priceRow;
	}

	public void setPriceRow(PriceRowData priceRow) {
		this.priceRow = priceRow;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public boolean isPopular() {
		return popular;
	}

	public void setPopular(boolean popular) {
		this.popular = popular;
	}

	public boolean isPopularCount() {
		return popularCount;
	}

	public void setPopularCount(boolean popularCount) {
		this.popularCount = popularCount;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	
}
