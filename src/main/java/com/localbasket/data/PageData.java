package com.localbasket.data;

import java.util.List;

public class PageData {
	private Long id;
	
	private String pageCode;
	
	private List<MediaPositionData> mediaWithPositions;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPageCode() {
		return pageCode;
	}

	public void setPageCode(String pageCode) {
		this.pageCode = pageCode;
	}

	public List<MediaPositionData> getMediaWithPositions() {
		return mediaWithPositions;
	}

	public void setMediaWithPositions(List<MediaPositionData> mediaWithPositions) {
		this.mediaWithPositions = mediaWithPositions;
	}
}
